<?php

class ModelModuleMetaTagsCreated extends Model
{
    /*TODO Automatical created MYSQL DB in controller MetaTagsCreated*/
    public function insertForm($data)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "newsletter_item");
        //echo "<pre>";print_r($data['newsletter']);exit;
        foreach ($data['newsletter'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "newsletter_item SET 
				note = '" . $this->db->escape($value['note']) . "', 
				email = '" . $this->db->escape($value['email']) . "', 
				name = '" . $this->db->escape($value['name']) . "', 
				img = '" . $this->db->escape($value['img']) . "', 
				male = '" . $this->db->escape($value['male']) . "', 
				female = '" . $this->db->escape($value['female']) . "', 
				language_id = '" . (int)$language_id . "', 
				annotation = '" . $this->db->escape($value['annotation']) . "'");
        }
    }

    public function onlyTemplate()
    {
        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');
        $query = $this->db->query("	SELECT * FROM " . DB_PREFIX . "custom_seo ORDER BY template_id DESC");
        $templates = array();
        if ($query->num_rows) {
            foreach ($query->rows as $item_row) {
                $cat_info = $this->model_catalog_category->getCategory($item_row['category_id']);
                $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($item_row['manufacturer_id']);
                $templates[$item_row['type']][] = array(
                    'name' => $item_row['name'],
                    'manufacturer_id' => $item_row['manufacturer_id'],
                    'category_id' => $item_row['category_id'],
                    'status' => $item_row['status'],
                    'type' => $item_row['type'],
                    'template_id' => $item_row['template_id'],
                    'category_info' => $cat_info,
                    'manufacturer_info' => $manufacturer_info,
                    'edit' => $this->url->link('module/metatags_create', 'token=' . $this->session->data['token'] . "&created=1&template_id=" . $item_row['template_id'], 'SSL')
                );
            }
        }
        return $templates;
    }

    public function getTemplate()
    {
        $query = $this->db->query("	SELECT * FROM " . DB_PREFIX . "custom_seo cs 
									LEFT JOIN " . DB_PREFIX . "custom_seo_description csd ON(cs.template_id=csd.template_id) 
									WHERE csd.language_id = '" . $this->config->get('config_language_id') . "' ORDER BY cs.template_id DESC");
        $templates = array();
        if ($query->num_rows) {
            foreach ($query->rows as $item_row) {
                $templates[$item_row['type']][$item_row['template_id']][] = array(
                    'name' => $item_row['name'],
                    'manufacturer_id' => $item_row['manufacturer_id'],
                    'category_id' => $item_row['category_id'],
                    'field' => $item_row['field'],
                    'type' => $item_row['type'],
                    'status' => $item_row['status'],
                    'template_id' => $item_row['template_id'],
                    'template' => $item_row['template'],
                );
            }
        }
        return $templates;
    }

    public function getTemplateById($template_id)
    {
        $query = $this->db->query("	SELECT * FROM " . DB_PREFIX . "custom_seo cs 
									LEFT JOIN " . DB_PREFIX . "custom_seo_description csd ON(cs.template_id=csd.template_id) 
									WHERE cs.template_id = '" . $template_id . "' ORDER BY cs.template_id DESC");
        $templates = array();

        if ($query->num_rows) {
            $templates['template'] = array(
                'name' => $query->rows[0]['name'],
                'manufacturer_id' => $query->rows[0]['manufacturer_id'],
                'category_id' => $query->rows[0]['category_id'],
                'status' => $query->rows[0]['status'],
                'status' => $query->rows[0]['status'],
                'type' => $query->rows[0]['type']
            );
            foreach ($query->rows as $item_row) {
                $templates['item'][$item_row['language_id']][$item_row['field']] = $item_row['template'];
            }
        }
        return $templates;
    }


    public function addTemplate($data)
    {
        $query = $this->db->query("	INSERT INTO " . DB_PREFIX . "custom_seo 
									SET `type` = '" . $this->db->escape($data['type']) . "', 
									`name` = '" . $this->db->escape($data['name']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', 
									category_id = '" . (int)$data['category_id'] . "', 
									status = '" . (int)$data['status'] . "' ");
        $template_id = $this->db->getLastId();
        foreach ($data['template'] as $language_id => $item_desck) {
            foreach ($item_desck as $key => $value) {
                $query = $this->db->query("	INSERT INTO " . DB_PREFIX . "custom_seo_description 
											SET language_id = '" . (int)$language_id . "', 
											field = '" . $key . "', 
											template_id = '" . $template_id . "', 
											template = '" . $value . "' ");
            }
        }
    }

    public function editTemplate($data)
    {
        $this->db->query("	UPDATE " . DB_PREFIX . "custom_seo 
							SET `type` = '" . $this->db->escape($data['type']) . "', 
							`name` = '" . $this->db->escape($data['name']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', 
							category_id = '" . (int)$data['category_id'] . "', 
							status = '" . (int)$data['status'] . "' WHERE template_id = '" . (int)$data['template_id'] . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "custom_seo_description WHERE template_id = '" . (int)$data['template_id'] . "'");
        foreach ($data['template'] as $language_id => $item_desck) {
            foreach ($item_desck as $key => $value) {
                $query = $this->db->query("	INSERT INTO " . DB_PREFIX . "custom_seo_description 
											SET language_id = '" . (int)$language_id . "', 
											field = '" . $key . "', 
											template_id = '" . (int)$data['template_id'] . "', 
											template = '" . $value . "' ");
            }
        }
    }

    public function deleteTemplate($template_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "custom_seo WHERE template_id = '" . (int)$template_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "custom_seo_description WHERE template_id = '" . (int)$template_id . "'");
    }

    public function getProductByCategoryManufacturer($category_id, $manufacturer_id)
    {
        $sql = "	SELECT p.product_id  FROM " . DB_PREFIX . "product p 
					LEFT JOIN " . DB_PREFIX . "product_to_category pc ON(p.product_id = pc.product_id) ";
        if ($category_id)
            $sql .= "WHERE pc.category_id = '" . (int)$category_id . "'";
        if ($manufacturer_id)
            $sql .= " AND p.manufacturer_id = '" . (int)$manufacturer_id . "'";
        $query = $this->db->query($sql);
        $product_id = array();
        foreach ($query->rows as $item_product) {
            $product_id[] = $item_product['product_id'];
        }
        return $product_id;
    }

    public function setTemplateProduct($template, $product_id, $manufacturer_id)
    {
        $product_info = $this->getProductDescriptions($product_id);
        $product_main_category_id = $this->getProductMainCategoryId($product_id);
        $product_caregory_info = $this->getCategoryDescriptions($product_main_category_id);
        $product_manufacturer_info = $this->getManufacturerDescriptions($manufacturer_id);
        $final_product_desck = array();
        foreach ($template as $key => $item_template) {
            $model = $product_info[$key]['model'];
            $price = $this->currency->format((float)$product_info[$key]['price']);
            $name = $product_info[$key]['name'];
            if (isset($product_caregory_info[$key]['name'])) {
                $caregory_name = $product_caregory_info[$key]['name'];
            } else {
                $caregory_name = "";
            }

            if (isset($product_manufacturer_info[$key]['name'])) {
                $manufacturer_name = $product_manufacturer_info[$key]['name'];
            } else {
                $manufacturer_name = "";
            }
            $search = array("%NAME%", "%BRAND%", "%CATEGORY%", "%MODEL%", "%PRICE%");
            $replace = array($name, $manufacturer_name, $caregory_name, $model, $price);
            /*FORMATED TEMPLATES*/
            $meta_title = $template[$key]['meta_title'];
            $meta_desck = $template[$key]['meta_desck'];
            $h1 = $template[$key]['h1'];
            $keywords = $template[$key]['keywords'];
            $name = $template[$key]['name'];
            $description = $template[$key]['description'];
            $tag = $template[$key]['tag'];

            $meta_title_new = str_replace($search, $replace, $meta_title);
            $meta_desck_new = str_replace($search, $replace, $meta_desck);
            $h1_new = str_replace($search, $replace, $h1);
            $keywords_new = str_replace($search, $replace, $keywords);
            $name_new = str_replace($search, $replace, $name);
            $description_new = str_replace($search, $replace, $description);
            $tag_new = str_replace($search, $replace, $tag);
            /*end FORMATED TEMPLATES*/

            /*FORMATED SEARCH*/
            $meta_title_format = $this->formatedString($meta_title_new);
            if ($meta_title_format == $product_info[$key]['meta_title'])
                $meta_title_format = $product_info[$key]['meta_title'];
            $meta_desck_format = $this->formatedString($meta_desck_new);
            if ($meta_desck_format == $product_info[$key]['meta_description'])
                $meta_desck_format = $product_info[$key]['meta_description'];
            $h1_format = $this->formatedString($h1_new);
            if ($h1_format == $product_info[$key]['meta_h1'])
                $h1_format = $product_info[$key]['meta_h1'];
            $keywords_format = $this->formatedString($keywords_new);
            if ($keywords_format == $product_info[$key]['meta_keyword'])
                $keywords_format = $product_info[$key]['meta_keyword'];
            $name_format = $this->formatedString($name_new);
            if ($name_format == $product_info[$key]['name'])
                $name_format = $product_info[$key]['name'];
            $description_format = $this->formatedString($description_new);
            if ($description_format == $product_info[$key]['description'])
                $description_format = $product_info[$key]['description'];
            $tag_format = $this->formatedString($tag_new);
            if ($tag_format == $product_info[$key]['tag'])
                $tag_format = $product_info[$key]['tag'];
            /*end FORMATED SEARCH*/
            $final_product_desck[$key] = array(
                'name' => $name ? $name_format : $product_info[$key]['name'],
                'description' => $description ? $description_format : $product_info[$key]['description'],
                'tag' => $tag ? $tag_format : $product_info[$key]['tag'],
                'meta_title' => $meta_title ? $meta_title_format : $product_info[$key]['meta_title'],
                'meta_h1' => $h1 ? $h1_format : $product_info[$key]['meta_h1'],
                'meta_description' => $meta_desck ? $meta_desck_format : $product_info[$key]['meta_description'],
                'meta_keyword' => $keywords ? $keywords_format : $product_info[$key]['meta_keyword']
            );

        }
        //echo "<pre>";print_r($final_product_desck);exit;
        $this->updateProductDescription($product_id, $final_product_desck);
    }

    public function setTemplateCategory($template, $category_id)
    {
        $category_info = $this->getCategoryDescriptions($category_id);
        $final_category_desck = array();
        foreach ($template as $key => $item_template) {
            $name = $category_info[$key]['name'];
            $description = $category_info[$key]['description'];
            $meta_title = $category_info[$key]['meta_title'];
            $meta_description = $category_info[$key]['meta_description'];
            $meta_keyword = $category_info[$key]['meta_keyword'];

            $search = array("%CATEGORY%");
            $replace = array($name);

            /*FORMATED TEMPLATES*/
            $meta_title = $template[$key]['meta_title'];
            $meta_desck = $template[$key]['meta_desck'];
            $h1 = $template[$key]['h1'];
            $keywords = $template[$key]['keywords'];
            $name = $template[$key]['name'];
            $description = $template[$key]['description'];

            $meta_title_new = str_replace($search, $replace, $meta_title);
            $meta_desck_new = str_replace($search, $replace, $meta_desck);
            $h1_new = str_replace($search, $replace, $h1);
            $keywords_new = str_replace($search, $replace, $keywords);
            $name_new = str_replace($search, $replace, $name);
            $description_new = str_replace($search, $replace, $description);
            /*end FORMATED TEMPLATES*/

            /*FORMATED SEARCH*/
            $meta_title_format = $this->formatedString($meta_title_new);
            if ($meta_title_format == $category_info[$key]['meta_title'])
                $meta_title_format = $category_info[$key]['meta_title'];
            $meta_desck_format = $this->formatedString($meta_desck_new);
            if ($meta_desck_format == $category_info[$key]['meta_description'])
                $meta_desck_format = $category_info[$key]['meta_description'];
            $h1_format = $this->formatedString($h1_new);
            if ($h1_format == $category_info[$key]['meta_h1'])
                $h1_format = $category_info[$key]['meta_h1'];
            $keywords_format = $this->formatedString($keywords_new);
            if ($keywords_format == $category_info[$key]['meta_keyword'])
                $keywords_format = $category_info[$key]['meta_keyword'];
            $name_format = $this->formatedString($name_new);
            if ($name_format == $category_info[$key]['name'])
                $name_format = $category_info[$key]['name'];
            $description_format = $this->formatedString($description_new);
            if ($description_format == $category_info[$key]['description'])
                $description_format = $category_info[$key]['description'];
            /*end FORMATED SEARCH*/
            $final_product_desck[$key] = array(
                'name' => $name ? $name_format : $category_info[$key]['name'],
                'description' => $description ? $description_format : $category_info[$key]['description'],
                'meta_title' => $meta_title ? $meta_title_format : $category_info[$key]['meta_title'],
                'meta_h1' => $h1 ? $h1_format : $category_info[$key]['meta_h1'],
                'meta_description' => $meta_desck ? $meta_desck_format : $category_info[$key]['meta_description'],
                'meta_keyword' => $keywords ? $keywords_format : $category_info[$key]['meta_keyword']
            );
        }
        $this->updateCategoryDescription($category_id, $final_product_desck);
    }

    public function formatedString($str)
    {
        $new_str = '';
        if ($str) {
            preg_match_all('/{{.*?}}/', $str, $m_string);
            if (isset($m_string[0][0])) {
                $string = str_replace(array("{{", "}}"), "", $m_string[0][0]);
                $var_string = explode("|", $string);
                $iteration = rand(0, count($var_string) - 1);
                $new_str = str_replace($m_string[0][0], $var_string[$iteration], $str);
            }
        }
        if ($new_str)
            return $new_str;
        else
            return $str;
    }

    public function getProductMainCategoryId($product_id)
    {
        $query = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "' AND main_category = '1' LIMIT 1");

        return ($query->num_rows ? (int)$query->row['category_id'] : 0);
    }

    public function getProductDescriptions($product_id)
    {
        $product_description_data = array();

        $query = $this->db->query("	SELECT * FROM " . DB_PREFIX . "product_description pd 
									LEFT JOIN " . DB_PREFIX . "product p ON(pd.product_id=p.product_id) 
									WHERE pd.product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'meta_title' => $result['meta_title'],
                'meta_h1' => $result['meta_h1'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'tag' => $result['tag'],
                'model' => $result['model'],
                'price' => $result['price']
            );
        }

        return $product_description_data;
    }

    public function getCategoryDescriptions($category_id)
    {
        $category_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'meta_title' => $result['meta_title'],
                'meta_h1' => $result['meta_h1'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'description' => $result['description']
            );
        }

        return $category_description_data;
    }

    public function getManufacturerDescriptions($manufacturer_id)
    {
        $manufacturer_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer_description WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

        foreach ($query->rows as $result) {
            $manufacturer_description_data[$result['language_id']] = array(
                'meta_title' => $result['meta_title'],
                'meta_h1' => $result['meta_h1'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'description' => $result['description']
            );
        }

        return $manufacturer_description_data;
    }

    public function getTemplateCatMan($cat_id, $manufacturer_id)
    {
        $query = $this->db->query("	SELECT * FROM " . DB_PREFIX . "custom_seo cs 
									LEFT JOIN " . DB_PREFIX . "custom_seo_description csd ON(cs.template_id=csd.template_id) 
									WHERE cs.category_id = '" . (int)$cat_id . "' AND cs.manufacturer_id = '" . (int)$manufacturer_id . "' ORDER BY csd.template_id DESC");
        $templates = array();

        if ($query->num_rows) {
            $templates['template'] = array(
                'name' => $query->rows[0]['name'],
                'manufacturer_id' => $query->rows[0]['manufacturer_id'],
                'category_id' => $query->rows[0]['category_id'],
                'status' => $query->rows[0]['status'],
                'status' => $query->rows[0]['status'],
                'type' => $query->rows[0]['type']
            );
            foreach ($query->rows as $item_row) {
                $templates['item'][$item_row['language_id']][$item_row['field']] = $item_row['template'];
            }
        }
        return $templates;
    }

    public function getTemplateCatType($cat_id, $type = 'category')
    {
        $query = $this->db->query("	SELECT * FROM " . DB_PREFIX . "custom_seo cs 
									LEFT JOIN " . DB_PREFIX . "custom_seo_description csd ON(cs.template_id=csd.template_id) 
									WHERE cs.category_id = '" . (int)$cat_id . "' AND cs.type = '" . $this->db->escape($type) . "' ORDER BY csd.template_id DESC");
        $templates = array();

        if ($query->num_rows) {
            $templates['template'] = array(
                'name' => $query->rows[0]['name'],
                'manufacturer_id' => $query->rows[0]['manufacturer_id'],
                'category_id' => $query->rows[0]['category_id'],
                'status' => $query->rows[0]['status'],
                'status' => $query->rows[0]['status'],
                'type' => $query->rows[0]['type']
            );
            foreach ($query->rows as $item_row) {
                $templates['item'][$item_row['language_id']][$item_row['field']] = $item_row['template'];
            }
        }
        return $templates;
    }

    public function updateProductDescription($product_id, $product_description)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($product_description as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_h1 = '" . $this->db->escape($value['meta_h1']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }
    }

    public function updateCategoryDescription($category_id, $category_description)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description WHERE category_id = '" . (int)$category_id . "'");

        foreach ($category_description as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }
    }
}