<?php echo $oc_header ?>
<div class="tab-pane active" id="tab-index">
    <?php /*
    <div class="row">
        <div class="col-sm-2">
            <ul class="nav nav-pills nav-stacked" id="tabs-vertical">
                <li class="active"><a href="#tab-info" data-toggle="tab"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $tab_instruction; ?></a></li>
                <?php $group_row = 1; ?>
                <li id="group-add"><a onclick="addOcfGroup();"><i class="fa fa-plus-circle" onclick=""></i>&nbsp;<?php echo $add_ocf_group; ?></a></li>
            </ul>
        </div>
        <div class="col-sm-10">
            <div class="tab-content">
                <div class="tab-pane active" id="tab-info">
                    <div class="form-group">
                        Інструкція заповнення
                    </div>
                </div>
            </div>
        </div>
    </div>*/?>
    <?php $for=1; ?>
    <?php $lan=1; ?>
    <?php $form=1; ?>
    <div class="row">
        <div class="col-sm-2">
            <ul class="nav nav-pills nav-stacked" id="tabs-vertical">
                <li><a href="#tab-make_order" data-toggle="tab">Сделать заказ</a></li>
                <li><a href="#tab-order_flowers" data-toggle="tab">Заказать цветы</a></li>
                <li><a href="#tab-feedback" data-toggle="tab">Остались вопросы</a></li>
            </ul>
        </div>
        <div class="col-sm-10">
            <div class="tab-content">
                <div class="tab-pane" id="tab-make_order">
                    <ul class="nav nav-tabs" id="tabs-forms<?php echo $form?>">
                        <li><a href="#tab-forms<?php echo $form?>-text" data-toggle="tab">Текст</a></li>
                        <li><a href="#tab-forms<?php echo $form?>-button" data-toggle="tab">Кнопка</a></li>
                        <li><a href="#tab-forms<?php echo $form?>-name" data-toggle="tab">Поле Имя</a></li>
                        <li><a href="#tab-forms<?php echo $form?>-phone" data-toggle="tab">Поле Телефон</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-forms<?php echo $form?>-text">
                            <div class="form-group">
                                <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                    <?php foreach ($languages as $language) { ?>
                                        <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php foreach ($languages as $language) { ?>
                                        <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                            <div class="form-group-group">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок формы</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][form_text][title]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['form_text']['title']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст на форме</label>
                                                    <div class="col-sm-10">
                                                        <textarea id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][form_text][text]" class="form-control" style="height: 150px"><?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['form_text']['text']; ?></textarea>
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                            <div class="form-group-group">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок про успешное заполнение</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][form_text][title_successful]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['form_text']['title_successful']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст про успешное заполнение</label>
                                                    <div class="col-sm-10">
                                                        <textarea id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][form_text][text_successful]" class="form-control" style="height: 150px"><?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['form_text']['text_successful']; ?></textarea>
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                            <div class="form-group-group">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок про ошибку при заполнении</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][form_text][title_error]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['form_text']['title_error']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php $lan++; ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-button">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">JS код для события onclick кнопки</label>
                                    <div class="col-sm-10">
                                        <textarea id="field<?php echo $for?>" name="oc[custom_forms][make_order][button][onclick]" class="form-control" style="height: 150px"><?php echo $oc['custom_forms']['make_order']['button']['onclick']; ?></textarea>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <div class="form-group">
                                    <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                        <?php foreach ($languages as $language) { ?>
                                            <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Назва кнопки</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][button][label]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['button']['label']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $lan++; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-name">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Обезательность</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][make_order][first_name][required]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['make_order']['first_name']['required']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Отправка на email</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][make_order][first_name][mail_send]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['make_order']['first_name']['mail_send']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <input type="hidden" name="oc[custom_forms][make_order][first_name][value]" value="" />
                                <div class="form-group">
                                    <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                        <?php foreach ($languages as $language) { ?>
                                            <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Label</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][first_name][label]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['first_name']['label']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Placeholder</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][first_name][placeholder]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['first_name']['placeholder']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст ошибки</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][first_name][error]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['first_name']['error']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $lan++; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-phone">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Обезательность</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][make_order][phone][required]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['make_order']['phone']['required']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Отправка на email</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][make_order][phone][mail_send]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['make_order']['phone']['mail_send']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <input type="hidden" name="oc[custom_forms][make_order][phone][value]" value="" />
                                <div class="form-group">
                                    <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                        <?php foreach ($languages as $language) { ?>
                                            <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Label</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][phone][label]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['phone']['label']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Placeholder</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][phone][placeholder]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['phone']['placeholder']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст ошибки</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][make_order][languages][<?php echo $language['language_id']; ?>][phone][error]" value="<?php echo $oc['custom_forms']['make_order']['languages'][$language['language_id']]['phone']['error']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $lan++; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $form++; ?>
                </div>
                <div class="tab-pane" id="tab-order_flowers">
                    <ul class="nav nav-tabs" id="tabs-forms<?php echo $form?>">
                        <li><a href="#tab-forms<?php echo $form?>-text" data-toggle="tab">Текст</a></li>
                        <li><a href="#tab-forms<?php echo $form?>-button" data-toggle="tab">Кнопка</a></li>
                        <li><a href="#tab-forms<?php echo $form?>-phone1" data-toggle="tab">Телефон получателя</a></li>
                        <li><a href="#tab-forms<?php echo $form?>-phone2" data-toggle="tab">Ваш телефон</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-forms<?php echo $form?>-text">
                            <div class="form-group">
                                <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                    <?php foreach ($languages as $language) { ?>
                                        <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php foreach ($languages as $language) { ?>
                                        <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                            <div class="form-group-group">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок формы</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][form_text][title]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['form_text']['title']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст на форме</label>
                                                    <div class="col-sm-10">
                                                        <textarea id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][form_text][text]" class="form-control" style="height: 150px"><?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['form_text']['text']; ?></textarea>
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                            <div class="form-group-group">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок про успешное заполнение</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][form_text][title_successful]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['form_text']['title_successful']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст про успешное заполнение</label>
                                                    <div class="col-sm-10">
                                                        <textarea id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][form_text][text_successful]" class="form-control" style="height: 150px"><?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['form_text']['text_successful']; ?></textarea>
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                            <div class="form-group-group">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок про ошибку при заполнении</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][form_text][title_error]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['form_text']['title_error']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php $lan++; ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-button">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">JS код для события onclick кнопки</label>
                                    <div class="col-sm-10">
                                        <textarea id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][button][onclick]" class="form-control" style="height: 150px"><?php echo $oc['custom_forms']['order_flowers']['button']['onclick']; ?></textarea>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <div class="form-group">
                                    <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                        <?php foreach ($languages as $language) { ?>
                                            <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Назва кнопки</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][button][label]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['button']['label']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $lan++; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-phone1">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Обезательность</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][order_flowers][phone1][required]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['order_flowers']['phone1']['required']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Отправка на email</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][order_flowers][phone1][mail_send]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['order_flowers']['phone1']['mail_send']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <input type="hidden" name="oc[custom_forms][order_flowers][phone1][value]" value="" />
                                <div class="form-group">
                                    <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                        <?php foreach ($languages as $language) { ?>
                                            <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Label</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][phone1][label]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['phone1']['label']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Placeholder</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][phone1][placeholder]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['phone1']['placeholder']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст ошибки</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][phone1][error]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['phone1']['error']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $lan++; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-phone2">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Обезательность</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][order_flowers][phone2][required]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['order_flowers']['phone2']['required']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Отправка на email</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][order_flowers][phone2][mail_send]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['order_flowers']['phone2']['mail_send']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <input type="hidden" name="oc[custom_forms][order_flowers][phone2][value]" value="" />
                                <div class="form-group">
                                    <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                        <?php foreach ($languages as $language) { ?>
                                            <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Label</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][phone2][label]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['phone2']['label']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Placeholder</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][phone2][placeholder]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['phone2']['placeholder']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст ошибки</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][order_flowers][languages][<?php echo $language['language_id']; ?>][phone2][error]" value="<?php echo $oc['custom_forms']['order_flowers']['languages'][$language['language_id']]['phone2']['error']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $lan++; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-image">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Фото 152*152 px</label>
                                    <div class="col-sm-10">
                                        <a href="" id="field<?php echo $for?>-thumb" data-toggle="image" class="img-thumbnail"><img src="<?php echo $oc['custom_forms']['order_flowers']['image']['thumb']; ?>" data-placeholder="<?php echo $oc['custom_forms']['order_flowers']['image']['thumb']; ?>" /></a>
                                        <input type="hidden" name="oc[custom_forms][order_flowers][image][value]" value="<?php echo $oc['custom_forms']['order_flowers']['image']['value']; ?>" id="field<?php echo $for?>-image" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $form++; ?>
                </div>
                <div class="tab-pane" id="tab-feedback">
                    <ul class="nav nav-tabs" id="tabs-forms<?php echo $form?>">
                        <li><a href="#tab-forms<?php echo $form?>-text" data-toggle="tab">Текст</a></li>
                        <li><a href="#tab-forms<?php echo $form?>-button" data-toggle="tab">Кнопка</a></li>
                        <li><a href="#tab-forms<?php echo $form?>-name" data-toggle="tab">Поле Имя</a></li>
                        <li><a href="#tab-forms<?php echo $form?>-phone" data-toggle="tab">Поле Телефон</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-forms<?php echo $form?>-text">
                            <div class="form-group">
                                <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                    <?php foreach ($languages as $language) { ?>
                                        <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <?php foreach ($languages as $language) { ?>
                                        <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                            <div class="form-group-group">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок формы</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][form_text][title]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['form_text']['title']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст на форме</label>
                                                    <div class="col-sm-10">
                                                        <textarea id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][form_text][text]" class="form-control" style="height: 150px"><?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['form_text']['text']; ?></textarea>
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                            <div class="form-group-group">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок про успешное заполнение</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][form_text][title_successful]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['form_text']['title_successful']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст про успешное заполнение</label>
                                                    <div class="col-sm-10">
                                                        <textarea id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][form_text][text_successful]" class="form-control" style="height: 150px"><?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['form_text']['text_successful']; ?></textarea>
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                            <div class="form-group-group">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Заголовок про ошибку при заполнении</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][form_text][title_error]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['form_text']['title_error']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php $lan++; ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-button">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">JS код для события onclick кнопки</label>
                                    <div class="col-sm-10">
                                        <textarea id="field<?php echo $for?>" name="oc[custom_forms][feedback][button][onclick]" class="form-control" style="height: 150px"><?php echo $oc['custom_forms']['feedback']['button']['onclick']; ?></textarea>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <div class="form-group">
                                    <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                        <?php foreach ($languages as $language) { ?>
                                            <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Назва кнопки</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][button][label]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['button']['label']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $lan++; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-name">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Обезательность</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][feedback][first_name][required]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['feedback']['first_name']['required']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Отправка на email</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][feedback][first_name][mail_send]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['feedback']['first_name']['mail_send']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <input type="hidden" name="oc[custom_forms][feedback][first_name][value]" value="" />
                                <div class="form-group">
                                    <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                        <?php foreach ($languages as $language) { ?>
                                            <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Label</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][first_name][label]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['first_name']['label']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Placeholder</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][first_name][placeholder]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['first_name']['placeholder']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст ошибки</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][first_name][error]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['first_name']['error']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $lan++; ?>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-forms<?php echo $form?>-phone">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Обезательность</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][feedback][phone][required]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['feedback']['phone']['required']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Отправка на email</label>
                                    <div class="col-sm-10">
                                        <select name="oc[custom_forms][feedback][phone][mail_send]" id="field<?php echo $for?>" class="form-control">
                                            <?php if ($oc['custom_forms']['feedback']['phone']['mail_send']) { ?>
                                                <option value="1" selected="selected">Да</option>
                                                <option value="0">Нет</option>
                                            <?php } else { ?>
                                                <option value="1">Да</option>
                                                <option value="0" selected="selected">Нет</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php $for++; ?>
                                </div>
                                <input type="hidden" name="oc[custom_forms][feedback][phone][value]" value="" />
                                <div class="form-group">
                                    <ul class="nav nav-tabs" id="languages<?php echo $lan; ?>">
                                        <?php foreach ($languages as $language) { ?>
                                            <li><a href="#language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <div class="tab-content">
                                        <?php foreach ($languages as $language) { ?>
                                            <div class="tab-pane" id="language<?php echo $lan; ?>_<?php echo $language['language_id']; ?>">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Label</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][phone][label]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['phone']['label']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Placeholder</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][phone][placeholder]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['phone']['placeholder']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="field<?php echo $for?>">Текст ошибки</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" id="field<?php echo $for?>" name="oc[custom_forms][feedback][languages][<?php echo $language['language_id']; ?>][phone][error]" value="<?php echo $oc['custom_forms']['feedback']['languages'][$language['language_id']]['phone']['error']; ?>" class="form-control" />
                                                    </div>
                                                    <?php $for++; ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php $lan++; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $form++; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#tabs a:first').tab('show');
    $('#tabs-vertical a:first').tab('show');
    <?php for($i=1; $i<=$form; $i++) { ?>
    $('#tabs-forms<?php echo $i?> a:first').tab('show');
    <?php } ?>
    <?php for($i=1; $i<=$lan; $i++) { ?>
    $('#languages<?php echo $i;?> a:first').tab('show');
    <?php } ?>
</script>
<?php echo $oc_footer ?>