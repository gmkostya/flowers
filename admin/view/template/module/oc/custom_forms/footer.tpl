                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div style="text-align:center; opacity: .5">
            <p>"#OpenCart# - Custom Forms" Version <?php echo $_version ?></p>
            <a href="https://web-systems.solutions/" class="webstudio_link" target="_blank">
                <img src="/system/library/wsh/ws.svg" alt="" class="img-responsive">
            </a>
        </div>
    </div>
<style>
        .form-group .webstudio_link{
            color: #666666 !important;
            text-decoration: none !important;
            position: relative;
            display: inline-block;
            padding-left: 25px;
            font-size: 12px;
            margin: 0;
            font-weight: 600;
            line-height: 1em;
        }
        .form-group .webstudio_link-icon{
            display: block;
            width: 20px;
            height: 20px;
            border-radius: 50%;
            border: 1px solid #515151;
            position: absolute;
            left: 0;
            top: 50%;
            margin-top: -10px;
        }
        .form-group .webstudio_link-icon span{
            display: block;
            position: absolute;
            width: 4px;
            height: 4px;
            border-radius: 50%;
            background-color: #515151;
        }
        .form-group .webstudio_link-icon span:nth-child(1){
            left: 4px;
            top: 4px;
        }
        .form-group .webstudio_link-icon span:nth-child(2){
            right: 4px;
            top: 4px;
        }
        .form-group .webstudio_link-icon span:nth-child(3){
            left: 4px;
            bottom: 4px;
        }
        .form-group .webstudio_link-icon span:nth-child(4){
            right: 4px;
            bottom: 4px;
        }
        .form-group .webstudio_link-name{
            text-transform: uppercase;
            letter-spacing: 0.5px;
        }
        .form-horizontal .form-group{
            margin-left: 0;
            margin-right: 0;
        }
        .form-group-group{
            border: 1px solid #1e91cf;
        }
        .form-group-group + .form-group-group{
            border-top: 0;
        }
</style>
<script type="text/javascript">
    $('.summernote').summernote({height: 300});

    <?php /*for($i=1; $i<=$lan; $i++) { ?>
    $('#languages<?php echo $i;?> a:first').tab('show');
    <?php }*/ ?>
    /*
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });*/
</script>
<?php echo $footer; ?>