<?php echo $header; ?>
<?php echo $column_left; ?>
    <div id="content">
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">
                    <button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if(!empty($_error_warning)) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>
                    <?php echo $_error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <?php if(!empty($_success)) { ?>
                <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i>
                    <?php echo $_success; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?> <small>(Если вы внесли какие-либо изменения, перед изменением вкладки нажмите кнопку "Сохранить")</small></h3>
                </div>
                <div class="panel-body">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-wsh" class="form-horizontal">
                        <ul class="nav nav-tabs" id="tabs">
                            <li<?php echo ($tab_active=='index') ? ' class="active"' : '' ?>><a href="<?php echo $tab_index_url; ?>"><i class="fa fa-cog"></i> <?php echo $tab_index?></a></li>
                            <li<?php echo ($tab_active=='about') ? ' class="active"' : '' ?>><a href="<?php echo $tab_about_url; ?>"><i class="fa fa-info-circle"></i> <?php echo $tab_about?></a></li>
                            <?php /*<li style="display: block; float:left; padding: 10px 0 0 2px;">« Если вы внесли какие-либо изменения, перед изменением вкладки нажмите кнопку "Сохранить"</li>*/?>
                        </ul>
                        <?php $for=1; ?>
                        <?php $lan=1; ?>
                        <div class="tab-content">