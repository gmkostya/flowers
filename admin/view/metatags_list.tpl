<?php echo $header; ?><?php echo $column_left; ?>
    <div id="content">

        <div class="container-fluid">
            <div class="page-header">
                <h1><?php echo $heading_title; ?></h1>
                <div class="container-fluid">
                    <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-information').submit() : false;"><i class="fa fa-trash-o"></i></button>
                        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
                    </div>
                    <ul class="breadcrumb">
                        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <?php if ($success) { ?>
                <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
                </div>
                <div class="panel-body">
                    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-information">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-templates-product" data-toggle="tab">Шаблоны продуктов</a></li>
                            <li><a href="#tab-templates-category" data-toggle="tab">Шаблоны категорий</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-templates-product">

                                <?php if(isset($templates['product'])){ ?>
                                    <a  id="button-regenerateproduct"  class="btn btn-default" onclick="regenerateproduct();">перегенерить все шаблоны продуктов</a>
<br/>
<br/>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                                <td class="text-left">Название Шаблона</td>
                                                <td class="text-left">Категория</td>
                                                <td class="text-left">Производитель</td>
                                                <td class="text-right">Тип шаблона</td>
                                                <td class="text-right">Статус</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($templates['product'] as $template) { ?>
                                                <tr class="js-product-tmpl-<?php echo $template['template_id']; ?>">
                                                    <td class="text-center"><input type="checkbox" name="selected[]" value="<?php echo $template['template_id']; ?>"/></td>
                                                    <td class="text-left"><?php echo $template['name']; ?></td>
                                                    <td class="text-right"><?php echo $template['category_info']['name']; ?></td>
                                                    <td class="text-right"><?php echo isset($template['manufacturer_info']['name']) ? $template['manufacturer_info']['name'] : ''; ?></td>
                                                    <td class="text-right"><?php echo $template['type']; ?></td>
                                                    <td class="text-right"><?php echo $template['status'] ? '<span class="label label-success">Включен</td>' : '<span class="label label-danger">Отключен</span>'; ?></td>
                                                    <td class="text-right">
                                                        <a href="<?php echo $template['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } else { ?>
                                    <div class="text-center alert alert-warning">
                                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                        <?php echo $text_no_results; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="tab-pane" id="tab-templates-category">
                                <?php if(isset($templates['category'])){ ?>

                                    <a id="button-regeneratecategory" class="btn btn-default" onclick="regeneratecategory();">перегенерить все шаблоны категорий</a>
                                    <br/>
                                    <br/>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                                <td class="text-left">Название Шаблона</td>
                                                <td class="text-left">Категория</td>
                                                <td class="text-right">Тип шаблона</td>
                                                <td class="text-right">Статус</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($templates['category'] as $template) { ?>
                                                <tr class="js-category-tmpl-<?php echo $template['template_id']; ?>">
                                                    <td class="text-center"><input type="checkbox" name="selected[]" value="<?php echo $template['template_id']; ?>"/></td>
                                                    <td class="text-left"><?php echo $template['name']; ?></td>
                                                    <td class="text-right"><?php echo $template['category_info']['name']; ?></td>
                                                    <td class="text-right"><?php echo $template['type']; ?></td>
                                                    <td class="text-right"><?php echo $template['status'] ? '<span class="label label-success">Включен</td>' : '<span class="label label-danger">Отключен</span>'; ?></td>
                                                    <td class="text-right">
                                                        <a href="<?php echo $template['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } else { ?>
                                    <div class="text-center alert alert-warning">
                                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                        <?php echo $text_no_results; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $('#type').change(function(){
            var type = $(this).val();
            if(type == 'category'){
                $('#manufacturer_id').slideToggle();
                $('#category_id').removeAttr('class').addClass('col-md-12');
                $('#tag_tr').slideToggle();
                $('#rowspan_table').removeAttr('rowspan').attr('rowspan', '6');
                $('#span_description_product').slideToggle();
                $('#span_description_category').slideToggle();
            } else {
                $('#manufacturer_id').slideToggle();
                $('#category_id').removeAttr('class').addClass('col-md-6 col-sm-6');
                $('#tag_tr').slideToggle();
                $('#rowspan_table').removeAttr('rowspan').attr('rowspan', '7');
                $('#span_description_product').slideToggle();
                $('#span_description_category').slideToggle();
            }
        });
        function updateProgress(percentage){
            if(percentage > 100) percentage = 100;
            $('.progress-bar').attr('aria-valuenow', percentage);
            $('.progress-bar').css('width', percentage+'%');
            $('.progress-bar').html(percentage+'%');
        }

        function regenerateproduct(){

              setTimeout(function(){
                $.ajax({
                    url: 'index.php?route=module/metatags_create/regenerateproduct&token=<?php echo $token; ?>',
                    dataType: 'json',
                    type: 'get',
                    beforeSend: function() {
                        $('#button-regenerateproduct').append('<i class="fa fa-circle-o-notch fa-spin"></i>');
                    },
                    success: function(json) {
                        for (var key in json.templates) {
                            $('.js-product-tmpl-'+key).css('background-color','#86cc93');
                        }
                        $('#button-regenerateproduct i').remove();
                    }
                });
            }, 1000)
        }

        function regeneratecategory(){

              setTimeout(function(){
                $.ajax({
                    url: 'index.php?route=module/metatags_create/regeneratecategory&token=<?php echo $token; ?>',
                    dataType: 'json',
                    type: 'get',
                    beforeSend: function() {
                        $('#button-regeneratecategory').append('<i class="fa fa-circle-o-notch fa-spin"></i>');
                    },
                    success: function(json) {
                        for (var key in json.templates) {
                            $('.js-category-tmpl-'+key).css('background-color','#86cc93');
                        }
                        $('#button-regeneratecategory i').remove();
                    }
                });
            }, 1000)
        }
    </script>

<?php echo $footer; ?>
