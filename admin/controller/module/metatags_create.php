<?php
class ControllerModuleMetatagsCreate extends Controller {
    private $error = array();
    private $form;
    private $data = array();


    public function index() {
        $this->load->language('module/metatags_create');
        $this->load->model('module/meta_tags_created');

        if(isset($this->request->get['created'])){
            $this->created_metatags();
            $this->document->setTitle($this->language->get('heading_title2'));
        } elseif(isset($this->request->get['list'])){
            $this->list_metatags();
            $this->document->setTitle($this->language->get('heading_title_list2'));
        } else {
            $this->list_metatags();
            $this->document->setTitle($this->language->get('heading_title_list2'));
        }
    }

    public function created_metatags(){
        if (isset($this->session->data['warning'])) {
            $data['error_warning'] = $this->session->data['warning'];
            unset($this->session->data['success']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if(isset($this->request->get['template_id'])){
            $template_id = (int)$this->request->get['template_id'];
            $data['template_id'] = $template_id;
            $data['template'] = $this->model_module_meta_tags_created->getTemplateById($template_id);
        }

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            //echo "<pre>";print_r($this->request->post);exit;
            if(isset($template_id)) {
                $this->model_module_meta_tags_created->editTemplate( $this->request->post );
            } else {
                $this->model_module_meta_tags_created->addTemplate( $this->request->post );
            }
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('module/metatags_create', 'token=' . $this->session->data['token'], 'SSL'));
        }
        $filter_data = array(
            'sort'        => 'name',
            'order'       => 'ASC'
        );
        $this->load->model('catalog/category');
        $data['categories'] = $this->model_catalog_category->getCategories($filter_data);
        $this->load->model('catalog/manufacturer');
        $data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();

        $data['heading_title'] = $this->language->get('heading_title3');
        $data['breadcrumbs'] = $this->getBreadCrumbs();
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['token'] = $this->session->data['token'];
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_get_key'] = $this->language->get('text_get_key');
        $data['text_unselect'] = $this->language->get('text_unselect');

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();
        $data['lang'] = $this->language->get('lang');
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if(isset($template_id)){
            $data['action'] = $this->url->link( 'module/metatags_create', 'token=' . $this->session->data['token'] . '&created=1&template_id=' . $template_id, 'SSL' );
        } else {
            $data['action'] = $this->url->link( 'module/metatags_create', 'token=' . $this->session->data['token'] . '&created=1', 'SSL' );
        }
        $data['cancel'] = $this->url->link('module/metatags_create', 'token=' . $this->session->data['token'], 'SSL');


        $template = 'module/metatags_create.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view($template, $data));
    }

    public function list_metatags(){
        if (isset($this->session->data['warning'])) {
            $data['error_warning'] = $this->session->data['warning'];
            unset($this->session->data['success']);
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        $data['heading_title'] = $this->language->get('heading_title_list3');
        $data['breadcrumbs'] = $this->getBreadCrumbs(0);
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['token'] = $this->session->data['token'];


        /*GET templates*/
        $data['templates'] = $this->model_module_meta_tags_created->onlyTemplate();
        /*end GET templates*/


        $data['add'] = $this->url->link('module/metatags_create', 'token=' . $this->session->data['token'] . "&created=1", 'SSL');
        $data['delete'] = $this->url->link('module/metatags_create/delete', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');


        $template = 'module/metatags_list.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view($template, $data));
    }

    private function getBreadCrumbs($index = 1) {
        $breadcrumbs = array();

        $breadcrumbs[] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $breadcrumbs[] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        if($index){
            $breadcrumbs[] = array(
                'text'      => $this->language->get('heading_title'),
                'href'      => $this->url->link('module/metatags_create', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => ' :: '
            );
        } else {
            $breadcrumbs[] = array(
                'text'      => $this->language->get('heading_title_list'),
                'href'      => $this->url->link('module/metatags_create', 'token=' . $this->session->data['token'] . "&created=1", 'SSL'),
                'separator' => ' :: '
            );
        }

        return $breadcrumbs;
    }

    public function delete(){
        $this->load->language('module/metatags_create');
        $this->load->model('module/meta_tags_created');
        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $template_id) {
                $this->model_module_meta_tags_created->deleteTemplate($template_id);
            }

            $this->session->data['success'] = $this->language->get('text_success_delete');

            $this->response->redirect($this->url->link('module/metatags_create', 'token=' . $this->session->data['token'], 'SSL'));
        }
    }

    public function generate($action = 'product'){
        $json = array();
        $step = 20;
        if(isset($this->request->post['template_id'])){
            $this->load->model('module/meta_tags_created');
            $template_id = (int)$this->request->post['template_id'];
            /*GET All Template info*/
            $template_info = $this->model_module_meta_tags_created->getTemplateById($template_id);
            /*end GET All Template info*/
            if($template_info['template']['status'] == 1){
                //if(isset($this->request->get['page_parse'])) {
                if ($template_info['template']['type'] == 'product') {
                    $this->load->model('catalog/product');
                    $this->load->model('catalog/category');
                    $this->load->model('catalog/manufacturer');
                    $category_id = $template_info['template']['category_id'];
                    $manufacturer_id = $template_info['template']['manufacturer_id'];
                    $products_id = $this->model_module_meta_tags_created->getProductByCategoryManufacturer($category_id, $manufacturer_id);
                    foreach($products_id as $item_product){
                        $this->model_module_meta_tags_created->setTemplateProduct($template_info['item'], $item_product, $manufacturer_id);
                    }
                    $json['success'] = 1;
                } elseif($template_info['template']['type'] == 'category') {
                    $this->load->model('catalog/product');
                    $this->load->model('catalog/category');
                    $this->load->model('catalog/manufacturer');
                    $category_id = $template_info['template']['category_id'];
                    $this->model_module_meta_tags_created->setTemplateCategory($template_info['item'], $category_id);
                }
            } else {
                $json['error']['status'] = 1;
            }
        } else {
            $json['error']['template_id'] = 1;
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }


    public function parse() {
        $json = array();
        $step = 20;
        if(isset($this->request->get['page_parse'])){
            $this->load->model('catalog/product');
            $this->load->model('catalog/category');
            $this->load->model('catalog/manufacturer');
            $this->load->model('tool/metatags_create');
            $total_products = $this->model_catalog_product->getTotalProducts();

            $pages = $total_products/$step;
            $page = $this->request->get['page_parse'];

            if($page <= ($pages+1)){


                $filter_data = array(
                    'start'		=> ($page - 1) * $step,
                    'limit'		=> $step
                );
                $products = $this->model_catalog_product->getProducts($filter_data);
                $setting = $this->config->get("metatags_create");
                //var_dump($setting);
                $i = 0;
                $j = 0;
                foreach($products as $product){

                    $product_id = $product['product_id'];

                    $this->load->model('localisation/language');
                    $languages = $this->model_localisation_language->getLanguages();
                    $data['lang'] = $this->language->get('lang');

                    $data_product_description = $this->model_catalog_product->getProductDescriptions($product_id);
                    $product_main_category_id = $this->model_catalog_product->getProductMainCategoryId($product_id);
                    $product_caregory_description = $this->model_catalog_category->getCategoryDescriptions($product_main_category_id);

                    $product_manufacturer_description = $this->model_catalog_manufacturer->getManufacturerDescriptions($product['manufacturer_id']);

                    $model = $product['model'];
                    $price = $this->currency->format((float)$product['price']);

                    //var_dump($price); exit;
                    $product_description_update = array();
                    foreach($languages as $language){
                        $name 				= $data_product_description[$language['language_id']]['name'];
                        $description 		= $data_product_description[$language['language_id']]['description'];
                        $meta_title 		= $data_product_description[$language['language_id']]['meta_title'];
                        $meta_h1 			= $data_product_description[$language['language_id']]['meta_h1'];
                        $meta_description 	= $data_product_description[$language['language_id']]['meta_description'];
                        $meta_keyword 		= $data_product_description[$language['language_id']]['meta_keyword'];
                        $tag 				= $data_product_description[$language['language_id']]['tag'];

                        if(isset($product_caregory_description[$language['language_id']]['name'])){
                            $caregory_name = $product_caregory_description[$language['language_id']]['name'];
                        } else {
                            $caregory_name = "";
                        }

                        if(isset($product_manufacturer_description[$language['language_id']]['name'])){
                            $manufacturer_name		= $product_manufacturer_description[$language['language_id']]['name'];
                        } else {
                            $manufacturer_name = "";
                        }

                        $search = array("%NAME%","%BRAND%","%CATEGORY%","%MODEL%","%PRICE%");
                        $replace   = array($name, "$manufacturer_name", $caregory_name,$model,$price);

                        $product_1 = $setting[$language['language_id']]['product_1'];
                        $product_1_new_srting = str_replace($search, $replace, $product_1);

                        preg_match_all('/{{.*?}}/', $product_1, $m_string);
                        if(isset($m_string[0][0])){
                            $count_var = 0;
                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                if($count_var < count($var_string)){
                                    $count_var = count($var_string);
                                }
                            }

                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                $count_var_item = count($var_string);
                                if($count_var_item < $count_var){
                                    $i_c_f = 0;
                                    for($i_c = $count_var_item; $i_c < $count_var; $i_c++){
                                        $var_string[$i_c] = $var_string[$i_c_f];
                                        $i_c_f++; }
                                }
                                if(!isset($var_string[$i])){ $i = 0; }

                                $product_1_new_srting = str_replace($find, $var_string[$i], $product_1_new_srting);
                            }
                        }

                        $product_2 = $setting[$language['language_id']]['product_2'];
                        $product_2_new_srting = str_replace($search, $replace, $product_2);

                        preg_match_all('/{{.*?}}/', $product_2, $m_string);
                        if(isset($m_string[0][0])){
                            $count_var = 0;
                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                if($count_var < count($var_string)){
                                    $count_var = count($var_string);
                                }
                            }
                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                $count_var_item = count($var_string);
                                if($count_var_item < $count_var){
                                    $i_c_f = 0;
                                    for($i_c = $count_var_item; $i_c < $count_var; $i_c++){
                                        $var_string[$i_c] = $var_string[$i_c_f];
                                        $i_c_f++; }
                                }

                                if(!isset($var_string[$j])){ $j = 0; }

                                $product_2_new_srting = str_replace($find, $var_string[$j], $product_2_new_srting);
                            }
                        }

                        $product_1_select_field = $setting[$language['language_id']]['product_1_select_field'];
                        if(($product_1_select_field != "0") && ($product_1_new_srting != "")){
                            $data_product_description[$language['language_id']][$product_1_select_field] = $product_1_new_srting;
                        }

                        $product_2_select_field = $setting[$language['language_id']]['product_2_select_field'];
                        if(($product_2_select_field != "0") && ($product_2_new_srting != "")){
                            $data_product_description[$language['language_id']][$product_2_select_field] = $product_2_new_srting;
                        }
                    }
                    $this->model_tool_metatags_create->updateProductDescription($product_id,$data_product_description);
                    $i++; $j++;
                }


                $json['text'] = "Страница ".$page." загружена";
                $json['total'] = ($page+1);
            } else {
                $json['text'] = "Обновлено";
                $json['total'] = 0;
            }


            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function parse_category() {
        $json = array();
        $step = 20;
        if(isset($this->request->get['page_parse'])){
            $this->load->model('catalog/product');
            $this->load->model('catalog/category');
            $this->load->model('catalog/manufacturer');
            $this->load->model('tool/metatags_create');
            $total_products = $this->model_catalog_category->getTotalCategories();

            $pages = $total_products/$step;
            $page = $this->request->get['page_parse'];

            if($page <= ($pages+1)){


                $filter_data = array(
                    'start'		=> ($page - 1) * $step,
                    'limit'		=> $step
                );
                $products = $this->model_catalog_category->getCategories($filter_data);
                $setting = $this->config->get("metatags_create");

                $i = 0;
                $j = 0;


                foreach($products as $product){

                    $category_id = $product['category_id'];

                    $this->load->model('localisation/language');
                    $languages = $this->model_localisation_language->getLanguages();
                    $data['lang'] = $this->language->get('lang');

                    $data_product_description = $this->model_catalog_category->getCategoryDescriptions($category_id);


                    $product_description_update = array();
                    foreach($languages as $language){
                        $name 				= $data_product_description[$language['language_id']]['name'];
                        $description 		= $data_product_description[$language['language_id']]['description'];
                        $meta_title 		= $data_product_description[$language['language_id']]['meta_title'];
                        $meta_description 	= $data_product_description[$language['language_id']]['meta_description'];
                        $meta_keyword 		= $data_product_description[$language['language_id']]['meta_keyword'];


                        if(isset($product_caregory_description[$language['language_id']]['name'])){
                            $caregory_name		= $product_caregory_description[$language['language_id']]['name'];
                        } else {
                            $caregory_name = "";
                        }



                        $search = array("%CATEGORY%");
                        $replace   = array($name);

                        $product_1 = $setting[$language['language_id']]['category_1'];
                        $product_1_new_srting = str_replace($search, $replace, $product_1);

                        preg_match_all('/{{.*?}}/', $product_1, $m_string);
                        if(isset($m_string[0][0])){

                            $count_var = 0;
                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                if($count_var < count($var_string)){
                                    $count_var = count($var_string);
                                }
                            }

                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                $count_var_item = count($var_string);
                                if($count_var_item < $count_var){
                                    $i_c_f = 0;
                                    for($i_c = $count_var_item; $i_c < $count_var; $i_c++){
                                        $var_string[$i_c] = $var_string[$i_c_f];
                                        $i_c_f++; }
                                }
                                if(!isset($var_string[$i])){ $i = 0; }

                                $product_1_new_srting = str_replace($find, $var_string[$i], $product_1_new_srting);
                            }
                        }

                        $product_2 = $setting[$language['language_id']]['category_2'];
                        $product_2_new_srting = str_replace($search, $replace, $product_2);

                        preg_match_all('/{{.*?}}/', $product_2, $m_string);
                        if(isset($m_string[0][0])){
                            $count_var = 0;
                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                if($count_var < count($var_string)){
                                    $count_var = count($var_string);
                                }
                            }
                            foreach($m_string[0] as $key => $find){
                                $string = str_replace(array("{{","}}"),"",$find);
                                $var_string = explode("|",$string);
                                $count_var_item = count($var_string);
                                if($count_var_item < $count_var){
                                    $i_c_f = 0;
                                    for($i_c = $count_var_item; $i_c < $count_var; $i_c++){
                                        $var_string[$i_c] = $var_string[$i_c_f];
                                        $i_c_f++; }
                                }

                                if(!isset($var_string[$j])){ $j = 0; }

                                $product_2_new_srting = str_replace($find, $var_string[$j], $product_2_new_srting);
                            }
                        }

                        $product_1_select_field = $setting[$language['language_id']]['category_1_select_field'];
                        if(($product_1_select_field != "0") && ($product_1_new_srting != "")){
                            $data_product_description[$language['language_id']][$product_1_select_field] = $product_1_new_srting;
                        }

                        $product_2_select_field = $setting[$language['language_id']]['category_2_select_field'];
                        if(($product_2_select_field != "0") && ($product_2_new_srting != "")){
                            $data_product_description[$language['language_id']][$product_2_select_field] = $product_2_new_srting;
                        }
                    }

                    $this->model_tool_metatags_create->updateCategoryDescription($category_id,$data_product_description);
                    $i++; $j++;
                }


                $json['text'] = "Страница ".$page." загружена";
                $json['total'] = ($page+1);
            } else {
                $json['text'] = "Обновлено";
                $json['total'] = 0;
            }


            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }


    public function generate_new_meta(){
        $json = array();
        if(isset($this->request->post)){
            echo "<pre>";print_r($this->request->post);exit;
            $step = 20;
            if(isset($this->request->get['page_parse'])){
                $this->load->model('catalog/product');
                $this->load->model('catalog/category');
                $this->load->model('catalog/manufacturer');
                $this->load->model('tool/metatags_create');
                $total_products = $this->model_catalog_product->getTotalProducts();

                $pages = $total_products/$step;
                $page = $this->request->get['page_parse'];

                if($page <= ($pages+1)){


                    $filter_data = array(
                        'start'		=> ($page - 1) * $step,
                        'limit'		=> $step
                    );
                    $products = $this->model_catalog_product->getProducts($filter_data);
                    $setting = $this->config->get("metatags_create");
                    //var_dump($setting);
                    $i = 0;
                    $j = 0;
                    foreach($products as $product){

                        $product_id = $product['product_id'];

                        $this->load->model('localisation/language');
                        $languages = $this->model_localisation_language->getLanguages();
                        $data['lang'] = $this->language->get('lang');

                        $data_product_description = $this->model_catalog_product->getProductDescriptions($product_id);
                        $product_main_category_id = $this->model_catalog_product->getProductMainCategoryId($product_id);
                        $product_caregory_description = $this->model_catalog_category->getCategoryDescriptions($product_main_category_id);

                        $product_manufacturer_description = $this->model_catalog_manufacturer->getManufacturerDescriptions($product['manufacturer_id']);

                        $model = $product['model'];
                        $price = $this->currency->format((float)$product['price']);

                        //var_dump($price); exit;
                        $product_description_update = array();
                        foreach($languages as $language){
                            $name 				= $data_product_description[$language['language_id']]['name'];
                            $description 		= $data_product_description[$language['language_id']]['description'];
                            $meta_title 		= $data_product_description[$language['language_id']]['meta_title'];
                            $meta_h1 			= $data_product_description[$language['language_id']]['meta_h1'];
                            $meta_description 	= $data_product_description[$language['language_id']]['meta_description'];
                            $meta_keyword 		= $data_product_description[$language['language_id']]['meta_keyword'];
                            $tag 				= $data_product_description[$language['language_id']]['tag'];

                            if(isset($product_caregory_description[$language['language_id']]['name'])){
                                $caregory_name = $product_caregory_description[$language['language_id']]['name'];
                            } else {
                                $caregory_name = "";
                            }

                            if(isset($product_manufacturer_description[$language['language_id']]['name'])){
                                $manufacturer_name		= $product_manufacturer_description[$language['language_id']]['name'];
                            } else {
                                $manufacturer_name = "";
                            }

                            $search = array("%NAME%","%BRAND%","%CATEGORY%","%MODEL%","%PRICE%");
                            $replace   = array($name, "$manufacturer_name", $caregory_name,$model,$price);

                            $product_1 = $setting[$language['language_id']]['product_1'];
                            $product_1_new_srting = str_replace($search, $replace, $product_1);

                            preg_match_all('/{{.*?}}/', $product_1, $m_string);
                            if(isset($m_string[0][0])){

                                $count_var = 0;
                                foreach($m_string[0] as $key => $find){
                                    $string = str_replace(array("{{","}}"),"",$find);
                                    $var_string = explode("|",$string);
                                    if($count_var < count($var_string)){
                                        $count_var = count($var_string);
                                    }
                                }

                                foreach($m_string[0] as $key => $find){
                                    $string = str_replace(array("{{","}}"),"",$find);
                                    $var_string = explode("|",$string);
                                    $count_var_item = count($var_string);
                                    if($count_var_item < $count_var){
                                        $i_c_f = 0;
                                        for($i_c = $count_var_item; $i_c < $count_var; $i_c++){
                                            $var_string[$i_c] = $var_string[$i_c_f];
                                            $i_c_f++; }
                                    }
                                    if(!isset($var_string[$i])){ $i = 0; }

                                    $product_1_new_srting = str_replace($find, $var_string[$i], $product_1_new_srting);
                                }
                            }

                            $product_2 = $setting[$language['language_id']]['product_2'];
                            $product_2_new_srting = str_replace($search, $replace, $product_2);

                            preg_match_all('/{{.*?}}/', $product_2, $m_string);
                            if(isset($m_string[0][0])){
                                $count_var = 0;
                                foreach($m_string[0] as $key => $find){
                                    $string = str_replace(array("{{","}}"),"",$find);
                                    $var_string = explode("|",$string);
                                    if($count_var < count($var_string)){
                                        $count_var = count($var_string);
                                    }
                                }
                                foreach($m_string[0] as $key => $find){
                                    $string = str_replace(array("{{","}}"),"",$find);
                                    $var_string = explode("|",$string);
                                    $count_var_item = count($var_string);
                                    if($count_var_item < $count_var){
                                        $i_c_f = 0;
                                        for($i_c = $count_var_item; $i_c < $count_var; $i_c++){
                                            $var_string[$i_c] = $var_string[$i_c_f];
                                            $i_c_f++; }
                                    }

                                    if(!isset($var_string[$j])){ $j = 0; }

                                    $product_2_new_srting = str_replace($find, $var_string[$j], $product_2_new_srting);
                                }
                            }

                            $product_1_select_field = $setting[$language['language_id']]['product_1_select_field'];
                            if(($product_1_select_field != "0") && ($product_1_new_srting != "")){
                                $data_product_description[$language['language_id']][$product_1_select_field] = $product_1_new_srting;
                            }

                            $product_2_select_field = $setting[$language['language_id']]['product_2_select_field'];
                            if(($product_2_select_field != "0") && ($product_2_new_srting != "")){
                                $data_product_description[$language['language_id']][$product_2_select_field] = $product_2_new_srting;
                            }
                        }
                        $this->model_tool_metatags_create->updateProductDescription($product_id,$data_product_description);
                        $i++; $j++;
                    }


                    $json['text'] = "Страница ".$page." загружена";
                    $json['total'] = ($page+1);
                } else {
                    $json['text'] = "Обновлено";
                    $json['total'] = 0;
                }


            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
