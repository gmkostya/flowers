<?php
class ControllerModuleGallery extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/gallery');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('gallery', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_banner_empty'] = sprintf($this->language->get('text_banner_empty'), $this->url->link('design/banner/add', 'token=' . $this->session->data['token'] ,'SSL'));

		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_banner'] = $this->language->get('entry_banner');
		$data['entry_limit'] = $this->language->get('entry_limit');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/gallery', 'token=' . $this->session->data['token'], 'SSL')
		);

        if (isset($this->request->post['gallery_banner_id'])) {
            $data['gallery_banner_id'] = $this->request->post['gallery_banner_id'];
        } else {
            $data['gallery_banner_id'] = $this->config->get('gallery_banner_id');
        }

        $this->load->model('design/banner');

        $data['banners'] = $this->model_design_banner->getBanners();

        $data['action'] = $this->url->link('module/gallery', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['gallery_status'])) {
			$data['gallery_status'] = $this->request->post['gallery_status'];
		} else {
			$data['gallery_status'] = $this->config->get('gallery_status');
		}

		if (isset($this->request->post['gallery_limit'])) {
			$data['gallery_limit'] = $this->request->post['gallery_limit'];
		} else {
			$data['gallery_limit'] = $this->config->get('gallery_limit');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/gallery.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/gallery')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}