<?php
class ControllerModuleOcCustomForms extends Controller {
	private $error    = array();
    private $_helper  = 'oc';
    private $_type	  = 'custom_forms';
    private $_module  = 'oc_custom_forms';
    private $_version = '0.1.181101';

    public function index() {
        $this->load->language('module/'.$this->_module);
        $this->document->setTitle($this->language->get('heading_title2'));
        $wsh = new WSH($this->_helper,$this->_type,$this->_helper);
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if(isset($this->request->post[$this->_helper][$this->_type])){
                foreach($this->request->post[$this->_helper][$this->_type] as $key => $value){
                    $wsh->wsh_set_group($key,$value);
                }
            }
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('module/'.$this->_module, 'token=' . $this->session->data['token'], 'SSL'));
        }
        //$data['add_oc_group'] = $this->language->get('add_oc_group');
        //$data['add_oc_field'] = $this->language->get('add_oc_group');
        //$data['tab_instruction'] = $this->language->get('tab_instruction');
        //$data['tab_group'] = $this->language->get('tab_group');

        //$this->load->model('localisation/language');
        //$data['languages'] = $this->model_localisation_language->getLanguages();

        /*if (isset($this->request->post[$this->_helper][$this->_type])) {
            $data[$this->_helper][$this->_type] = $this->request->post[$this->_helper][$this->_type];
        } else {
            //$data[$this->_helper][$this->_name] = wsh_get($this->_name);
            $data[$this->_helper][$this->_type] =$wsh->wsh_get_type($this->_type);
        }*/
        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();
        /*echo '<pre>';
        print_r($wsh->wsh_get_group());
        echo '</pre>';*/
        if (isset($this->request->post[$this->_helper][$this->_type])) {
            $data[$this->_helper][$this->_type] = $this->request->post[$this->_helper][$this->_type];
        } else {
            $data[$this->_helper][$this->_type] = $wsh->wsh_get_group();
        }
        $data['oc_header'] = $this->load->controller('module/'.$this->_module.'/header');
        $data['oc_footer'] = $this->load->controller('module/'.$this->_module.'/footer');

        $this->response->setOutput($this->load->view('module/'.$this->_helper.'/'.$this->_type.'/index.tpl', $data));
    }

    public function about() {
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('module/'.$this->_module.'/about', 'token=' . $this->session->data['token'], 'SSL'));
        }
        $data['_type'] = $this->_type.'/about';
        $data['_version'] = $this->_version;
        $data['oc_header'] = $this->load->controller('module/'.$this->_module.'/header');
        $data['oc_footer'] = $this->load->controller('module/'.$this->_module.'/footer');
        $this->response->setOutput($this->load->view('module/'.$this->_helper.'/'.$this->_type.'/about.tpl', $data));
    }

	public function header(){
        $this->load->language('module/'.$this->_module);
        $this->document->setTitle($this->language->get('heading_title2'));

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        //$data['button_add_module'] = $this->language->get('button_add_module');
        $data['button_remove'] = $this->language->get('button_remove');

        $data['heading_title'] = $this->language->get('heading_title2');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_content_top'] = $this->language->get('text_content_top');
        $data['text_content_bottom'] = $this->language->get('text_content_bottom');
        $data['text_column_left'] = $this->language->get('text_column_left');
        $data['text_column_right'] = $this->language->get('text_column_right');

        $data['entry_banner'] = $this->language->get('entry_banner');
        $data['entry_limit'] = $this->language->get('entry_limit');
        $data['entry_scroll'] = $this->language->get('entry_scroll');
        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_layout'] = $this->language->get('entry_layout');
        $data['entry_position'] = $this->language->get('entry_position');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title2'),
            'href'      => $this->url->link('module/'.$this->_module, 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $route_index = 'module/'.$this->_module;
        $route_settings = 'module/'.$this->_module.'/settings';
        $route_about = 'module/'.$this->_module.'/about';
        switch($this->request->get['route']){
            case $route_index: $data['tab_active'] = 'index'; break;
            case $route_settings: $data['tab_active'] = 'settings'; break;
            case $route_about: $data['tab_active'] = 'about'; break;
        }
        /*if($this->request->get['route'] == $route_ocf){
            $data['tab_active'] = $this->_type;
        }elseif($this->request->get['route'] == $route_about){
            $data['tab_active'] = 'about';
        }*/
        $data['tab_index'] = $this->language->get('tab_group');
        $data['tab_settings'] = $this->language->get('tab_settings');
        $data['tab_about'] = $this->language->get('tab_about');
        $data['tab_index_url'] = $this->url->link($route_index, 'token=' . $this->session->data['token'], 'SSL');
        $data['tab_settings_url'] = $this->url->link($route_settings, 'token=' . $this->session->data['token'], 'SSL');
        $data['tab_about_url'] = $this->url->link($route_about, 'token=' . $this->session->data['token'], 'SSL');

        $data['action'] = $this->url->link('module/'.$this->_module, 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $data['token'] = $this->session->data['token'];
        $data['_type'] = $this->_type;
        $data['_helper'] = $this->_helper;
        $data['_module'] = $this->_module;
        $data['_version'] = $this->_version;

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['header'] = $this->load->controller('common/header');

        return $this->load->view('module/'.$this->_helper.'/'.$this->_type.'/header.tpl', $data);
    }

    public function footer(){

        $data['_version'] = $this->_version;
        $data['footer'] = $this->load->controller('common/footer');

        return $this->load->view('module/'.$this->_helper.'/'.$this->_type.'/footer.tpl', $data);
    }
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/'.$this->_module)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		/*if (isset($this->request->post[''.$this->_module])) {
			foreach ($this->request->post['wk_settings'] as $key => $value) {
				if (!$value['width'] || !$value['height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}*/
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>