<?php
// Heading
$_['heading_title']    = 'Галерея';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified information module!';
$_['text_edit']        = 'Edit Information Module';
$_['text_banner_empty']        = 'You have not active banner <a href="%s">create banner</a>';


// Entry
$_['entry_status']     = 'Status';
$_['entry_banner']     = 'Select banner for gallery';
$_['entry_limit']     = 'Entry limit photo on page';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify information module!';