<?php
// Heading
$_['heading_title']  = '<span style="color: #804B95;font-weight: bold;font-size: 13px;"><img src="/admin/view/image/web_system_logo.png" alt="web-system" style="margin-right: 5px;width: 15px;margin-top: -4px;"> Metatags Product Create (<span style="color:#1AAE60;font-weight: bold;">web</span><span style="color:#F1C719;font-weight: bold;">-</span><span style="color:#804B95;font-weight: bold;">systems.</span><span style="color:#24B2C0;font-weight: bold;">solution</span>)</span>';
$_['heading_title2'] = 'Metatags Product Create';
$_['heading_title3'] = '<span style="color: #804B95;font-weight: bold;font-size: 18px;"><img src="/admin/view/image/web_system_logo.png" alt="web-system" style="margin-right: 5px;width: 21px;margin-top: -4px;"> Metatags Product Create (<span style="color:#1AAE60;font-weight: bold;">web</span><span style="color:#F1C719;font-weight: bold;">-</span><span style="color:#804B95;font-weight: bold;">systems.</span><span style="color:#24B2C0;font-weight: bold;">solution</span>)</span>';
$_['heading_title_list']  = '<span style="color: #804B95;font-weight: bold;font-size: 13px;"><img src="/admin/view/image/web_system_logo.png" alt="web-system" style="margin-right: 5px;width: 15px;margin-top: -4px;"> Metatags Product Create LIST (<span style="color:#1AAE60;font-weight: bold;">web</span><span style="color:#F1C719;font-weight: bold;">-</span><span style="color:#804B95;font-weight: bold;">systems.</span><span style="color:#24B2C0;font-weight: bold;">solution</span>)</span>';
$_['heading_title_list2'] = 'Metatags Product Create LIST';
$_['heading_title_list3'] = '<span style="color: #804B95;font-weight: bold;font-size: 18px;"><img src="/admin/view/image/web_system_logo.png" alt="web-system" style="margin-right: 5px;width: 21px;margin-top: -4px;"> Metatags Product Create LIST (<span style="color:#1AAE60;font-weight: bold;">web</span><span style="color:#F1C719;font-weight: bold;">-</span><span style="color:#804B95;font-weight: bold;">systems.</span><span style="color:#24B2C0;font-weight: bold;">solution</span>)</span>';

// Text
$_['text_module'] = 'Модули';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_success_delete'] = 'Шаблон удален';
$_['text_edit'] = 'Модуль Product Metatags Create';
$_['text_unisender'] = '<a onclick="window.open(\'http://www.unisender.com/?a=opencart\');"><img src="view/image/payment/unisender.png" alt="Unisender" title="Unisender" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_get_key'] = 'Зарегистрироваться и получить ключ';
$_['text_unselect'] = 'снять выделение';
$_['text_list'] = 'Список шаблонов';

// Entry
$_['entry_unisender_key']	= 'Ключ Metatags Create API:';
$_['entry_unisender_key_help'] = 'Доступен в личном кабинете Metatags Create';
$_['entry_unisender_subscribtion']	= 'Подписывать на рассылки:';
$_['entry_unisender_subscribtion_help']	= 'Покупатели, подписавшиеся на новости, будут подписаны на эти рассылки';
$_['entry_unisender_ignore'] = 'Всегда подписывать на рассылки, независимо от выбора покупателя';
$_['entry_unisender_status'] = 'Статус:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_form'] = 'Форма заполнена с ошибками!';
$_['error_empty_field'] = 'Это поле должно быть заполнено!';
