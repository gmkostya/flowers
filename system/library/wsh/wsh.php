<?php
class WSH{

    private $helper = '';
    private $type = '';
    private $file_type = '';
    private $result = array();
    public function __construct($helper='oc',$type='custom_fields',$file_type='oc') {
        $this->helper = $helper;
        $this->type = $type;
        $this->file_type = $file_type;
        if(!is_dir(DIR_SYSTEM . $this->helper . '/'. $this->type . '/')) {
            mkdir(DIR_SYSTEM . $this->helper . '/'. $this->type. '/', 0766, true);
        }
    }

    public function wsh_set_type($type, $value){
        $this->wsh_delete_type($type);
        $file = DIR_SYSTEM . $this->helper . '/'. $type . '.' . $this->file_type;
        $handle = fopen($file, 'w');
        flock($handle, LOCK_EX);
        $data = array();
        $data[$type] = $value;
        fwrite($handle, json_encode($data));
        fflush($handle);
        flock($handle, LOCK_UN);
        fclose($handle);
    }

    public function wsh_set_group($group, $value){
        $this->wsh_delete_group($group);
        $file = DIR_SYSTEM . $this->helper . '/'. $this->type . '/' . $group . '.' . $this->file_type;
        $handle = fopen($file, 'w');
        flock($handle, LOCK_EX);
        $data = array();
        $data[$group] = $value;
        fwrite($handle, json_encode($data));
        fflush($handle);
        flock($handle, LOCK_UN);
        fclose($handle);
    }

    private function get_group($group=''){
        if(empty($group)){
            $group = '*';
        }
        $files = glob(DIR_SYSTEM . $this->helper . '/'. $this->type . '/' . $group . '.' . $this->file_type);
        if ($files) {
            $result = array();
            foreach ($files as $file){
                $handle = fopen($file, 'r');
                flock($handle, LOCK_SH);
                $data = fread($handle, filesize($file));
                flock($handle, LOCK_UN);
                fclose($handle);
                $res = json_decode($data,true);
                $result = array_merge($result,$res);
            }
            return $result;
        }
        return false;
    }

    public function wsh_get_type($type){
        $files = glob(DIR_SYSTEM . $this->helper . '/'. $type . '.' . $this->file_type);
        if ($files) {
            $result = array();
            foreach ($files as $file){
                $handle = fopen($file, 'r');
                flock($handle, LOCK_SH);
                $data = fread($handle, filesize($file));
                flock($handle, LOCK_UN);
                fclose($handle);
                $res = json_decode($data,true);
                $result = array_merge($result,$res);
            }
            return $result;
        }
        return false;
    }

    public function wsh_get_group($group=''){
        return $this->get_group($group);
    }

    public function wsh_get_group_language($group,$language_id=0)
    {
        $result = array();
        if (!isset($result[$group])) {
            $oc_group = $this->get_group($group);
            if (is_array($oc_group[$group]) || is_object($oc_group[$group])){
                foreach ($oc_group[$group] as $key1 => $set) {
                    if($key1 == 'languages'){//якщо мовний масив
                        foreach ($set as $lang => $setting) {//мовний масив
                            if($language_id == $lang){//поточна мова
                                foreach ($setting as $key2 => $el) {
                                    if (is_array($el)) {
                                        $result[$group][$key2] = $el;
                                    }else{
                                        $result[$group][$key2] = html_entity_decode($el, ENT_QUOTES, 'UTF-8');
                                    }
                                }
                            }
                        }
                    }else{
                        if (is_array($set)) {
                            if (isset($result[$group][$key1])) {
                                $result[$group][$key1] = array_merge($result[$group][$key1], $set);
                            } else {
                                $result[$group][$key1] = $set;
                            }
                        } else {
                            if (isset($result[$group][$key1])) {
                                $result[$group][$key1] = array_merge($result[$group][$key1], html_entity_decode($set, ENT_QUOTES, 'UTF-8'));
                            } else {
                                $result[$group][$key1] = html_entity_decode($set, ENT_QUOTES, 'UTF-8');
                            }
                        }
                    }
                }
            }
        }
        return isset($result[$group]) ? $result[$group] : '#empty#';
    }

    private function wsh_delete_type($type){
        $files = glob(DIR_SYSTEM . $this->helper . '/'. $type . '.' . $this->file_type);
        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }

    private function wsh_delete_group($group){
        $files = glob(DIR_SYSTEM . $this->helper . '/'. $this->type . '/' . $group . '.' . $this->file_type);
        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }
}

?>