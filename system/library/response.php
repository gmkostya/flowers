<?php
class Response {
	private $headers = array();
	private $level = 0;
	private $output;

	public function addHeader($header) {
		$this->headers[] = $header;
	}

	public function redirect($url, $status = 302) {
		header('Location: ' . str_replace(array('&amp;', "\n", "\r"), array('&', '', ''), $url), true, $status);
		exit();
	}

	public function setCompression($level) {
		$this->level = $level;
	}

	public function setOutput($output) {
		$this->output = $output;
	}

	public function getOutput() {
		return $this->output;
	}

	private function compress($data, $level = 0) {
		if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') !== false)) {
			$encoding = 'gzip';
		}

		if (isset($_SERVER['HTTP_ACCEPT_ENCODING']) && (strpos($_SERVER['HTTP_ACCEPT_ENCODING'], 'x-gzip') !== false)) {
			$encoding = 'x-gzip';
		}

		if (!isset($encoding) || ($level < -1 || $level > 9)) {
			return $data;
		}

		if (!extension_loaded('zlib') || ini_get('zlib.output_compression')) {
			return $data;
		}

		if (headers_sent()) {
			return $data;
		}

		if (connection_status()) {
			return $data;
		}

		$this->addHeader('Content-Encoding: ' . $encoding);

		return gzencode($data, (int)$level);
	}
    
    private function delete_circular_reference($output) {
        return preg_replace_callback('/(\<a.*?href.*?=.*?("|\'))(.*?)(\2.*?>)/s', "self::checkUrl", $output);
    }

    private function checkUrl($match) {

        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }

        $full_url = $protocol.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

        if ($full_url{strlen($full_url)-1}=='/') {
            $full_url = substr($full_url, 0,strlen($full_url)-1);
        }

        $url = $_SERVER['REQUEST_URI'];

        if ($url{strlen($url)-1}=='/') {
            $url = substr($url, 0,strlen($url)-1);
        }

        if (preg_match('/^'.preg_quote($full_url,'/').'\/?$/', $match[3])||preg_match('/^'.preg_quote($url,'/').'\/?$/', $match[3])) {
            return $match[1].$match[3].$match[2].' rel='.$match[2].'nofollow'.$match[4];
        } else {

            $parse_url = parse_url($match[3]);

            if(isset($parse_url['host']) and ($parse_url['host'] != $_SERVER['SERVER_NAME']) ){
                return $match[1].$match[3].$match[2].' rel='.$match[2].'nofollow noopener'.$match[4];
            } else {
                return $match[0];
            }
        }
    }


	public function output() {
		if ($this->output) {
			if ($this->level) {
				$output = $this->compress($this->output, $this->level);
			} else {
				$output = $this->output;
			}
            
			if (!headers_sent()) {
				foreach ($this->headers as $header) {
					header($header, true);
				}
			}
            
			echo $this->delete_circular_reference($output);
		}
	}
}
