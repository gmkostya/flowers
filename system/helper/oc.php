<?php
    include_once(DIR_SYSTEM . 'library/wsh/wsh.php');
    if (!function_exists('oc_log')) {
        function oc_log($message,$file_name = 'oc_log',$file_type='log'){
            $file = DIR_LOGS . $file_name.'.'.$file_type;
            $handle = fopen($file, 'a');

            flock($handle, LOCK_EX);
            if(is_array($message)){
                fwrite($handle, date('Y-m-d G:i:s') . "\n" .print_r($message, true). "\n");
            }else{
                fwrite($handle, date('Y-m-d G:i:s') . ' - ' . $message. "\n");
            }
            fflush($handle);
            flock($handle, LOCK_UN);
            fclose($handle);
        }
    }

    if (!function_exists('oc_validate')) {
        function oc_validate($string = "", $filter = 2) {
            $filters = array(
                0 => FILTER_VALIDATE_BOOLEAN,
                1 => FILTER_VALIDATE_INT,
                2 => FILTER_VALIDATE_EMAIL,
                3 => FILTER_VALIDATE_URL
            );

            return filter_var($string, $filters[$filter]);
        }
    }

    /*if (!function_exists('set_ocf_type')) {
        function set_ocf_type($type, $value) {
            $helper = 'wsh';
            //$type = 'ocf';
            $res = new wsh($helper,$type,$helper);
            $res->wsh_set_type($type,$value);
        }
    }

    if (!function_exists('set_ocf_group')) {
        function set_ocf_group($group, $value) {
            $helper = 'wsh';
            $type = 'ocf';
            $res = new wsh($helper,$type,$helper);
            $res->wsh_set_type($group,$value);
        }
    }

    if(!function_exists('get_ocf_group')){
        function get_ocf_group($group,$language_id='')
        {
            //include_once(DIR_SYSTEM . 'library/wsh/wsh.php');
            $helper = 'wsh';
            $type = 'ocf';
            $res = new wsh($helper,$type,$helper);
            if(empty($language_id)){
                return $res->oc_get_group($group);
            }else{
                return $res->oc_get_group($group,$language_id);
            }

        }
    }*/

    if(!function_exists('oc_get_group')){
        function oc_get_group($group,$language_id='')
        {
            $helper = 'oc';
            $type = 'custom_fields';
            $res = new WSH($helper,$type,$helper);
            if(empty($language_id)){
                return $res->wsh_get_group($group);
            }else{
                return $res->wsh_get_group($group,$language_id);
            }
        }
    }

    if(!function_exists('oc_get_field')){
        function oc_get_field($field,$group,$language_id='')
        {
            $helper = 'oc';
            $type = 'custom_fields';
            $res = new wsh($helper,$type,$helper);
            if(empty($language_id)){
                return $res->wsh_get_field($field,$group);
            }else{
                return $res->wsh_get_field($field,$group,$language_id);
            }

        }
    }

    /*if (!function_exists('wsh_catalog')) {
        function wsh_catalog($type,$name,$language_id)
        {
            static $arrWsh = array();
            $wsh_catalog = wsh_get($type,$name);
            if (!isset($arrWsh[$name])) {
                foreach ($wsh_catalog[$name] as $key1 => $set) {
                    if($key1 == 'langs'){//якщо мовний масив
                        foreach ($set as $lang => $setting) {//мовний масив
                            if($language_id == $lang){//поточна мова
                                foreach ($setting as $key2 => $el) {
                                    if (is_array($el)) {
                                        $arrWsh[$name][$key2] = $el;
                                    }else{
                                        $arrWsh[$name][$key2] = html_entity_decode($el, ENT_QUOTES, 'UTF-8');
                                    }
                                }
                            }
                        }
                    }else{
                        if (is_array($set)) {
                            if (isset($arrWsh[$name][$key1])) {
                                $arrWsh[$name][$key1] = array_merge($arrWsh[$name][$key1], $set);
                            } else {
                                $arrWsh[$name][$key1] = $set;
                            }
                        } else {
                            if (isset($arrWsh[$name][$key1])) {
                                $arrWsh[$name][$key1] = array_merge($arrWsh[$name][$key1], html_entity_decode($set, ENT_QUOTES, 'UTF-8'));
                            } else {
                                $arrWsh[$name][$key1] = html_entity_decode($set, ENT_QUOTES, 'UTF-8');
                            }
                        }
                    }
                }
            }
            return isset($arrWsh[$name]) ? $arrWsh[$name] : '#empty#';
        }
    }

    if (!function_exists('wsh_get')) {
        function wsh_get($type,$key=''){
            if(empty($key)){
                $key = '*';
            }
            $files = glob(DIR_SYSTEM . 'wsh/' . $type . '/' . $key . '.wsh');
            if ($files) {
                $wsh = array();
                foreach ($files as $file){
                    $handle = fopen($file, 'r');
                    flock($handle, LOCK_SH);
                    $data = fread($handle, filesize($file));
                    flock($handle, LOCK_UN);
                    fclose($handle);
                    $res = json_decode($data,true);
                    $wsh = array_merge($wsh,$res);
                }
                return $wsh;
            }
            return false;
        }
    }

    if (!function_exists('wsh_set')) {
        function wsh_set($type, $key, $value) {
            if (!is_dir(DIR_SYSTEM . 'wsh/'. $type . '/')) {
                mkdir(DIR_SYSTEM . 'wsh/' . $type . '/', 0766, true);
            }
            wsh_delete($type,$key);
            $file = DIR_SYSTEM . 'wsh/' . $type . '/' . $key . '.wsh';
            $handle = fopen($file, 'w');
            flock($handle, LOCK_EX);
            $data = array();
            $data[$key] = $value;
            fwrite($handle, json_encode($data));
            fflush($handle);
            flock($handle, LOCK_UN);
            fclose($handle);
        }
    }

    if (!function_exists('wsh_delete')) {
        function wsh_delete($type, $key) {
            $files = glob(DIR_SYSTEM . 'wsh/' . $type . '/' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.wsh');
            if ($files) {
                foreach ($files as $file) {
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
            }
        }
    }*/

?>