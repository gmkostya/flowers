/*$(document).ready(function() {
	$('input[name="phone"]').mask("+7(999)999-99-99");
});*/

function werror() {
    //$.fancybox.close('All');
    $.fancybox.open($('#popap-error'));
    /*setTimeout(function () {
        $.fancybox.close('All');
    },3000);*/
}

jQuery(document).on('submit', ".oc-custom-forms", function(event) {
	event.preventDefault();
	jQuery.wsh.submitForm(this);

	/*if (jQuery(this).hasClass('ascroll')) {
		jQuery.wsh.scrollTo(this, 700);
	}*/
});

jQuery.wsh = {

	scrollTo: function(elem, speed){
		jQuery("html, body").animate({ scrollTop: jQuery(elem).offset().top - 30 }, speed);
	},

	submitForm: function(form)
	{
		var errors = '';

		jQuery.ajax({
			type: 'POST',
			url: form.action,
			data: jQuery(form).serialize(),
			dataType: 'json',
			beforeSend: function(xhr) {
				//jQuery(form).loadmask();
				jQuery(form).addClass('loading');
			},
			success: function(data)
			{
				jQuery(form).find('span.form-control-feedback').remove();
				jQuery(form).find('.form-error').remove();
				jQuery(form).find('.error').removeClass('error');

				if ('error' == data.status) {

					if (jQuery(form).hasClass('werror')) {
						errors = '';
						for (var key in data.errors) {
							errors += '<p>' + data.errors[key] + '</p>';
							if(key != 'rating'){
                                jQuery(form).find('[name=' + key + ']').addClass("error");
							}
						}
                        jQuery('#modal-error .modal-header h3').text(data.title);
                        jQuery('#modal-error .modal-body p').remove();
						jQuery('#modal-error .modal-body').append(errors);

						//$.fancybox.open($('#popap-error'));
                        //$('.modal').modal('hide');
						$('#modal-error').modal('show');

						//jQuery.wsh.resetForm(form);

					} else {
						for (var key in data.errors) {

                            errors = '<span class="error">' + data.errors[key] + '</span>';

                            jQuery(form).find('[name=' + key + ']').addClass("error");
							/*if (key == 'rating') {
								errors = '' +
									'<div class="form-error">' + data.errors[key] + '</div>';
								jQuery(form).find('.reviews-form__box-rating').addClass("has-error").append(errors);
							} else {
								errors = '<span class="material-icons form-control-feedback"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>' +
									'<span class="form-error">' + data.errors[key] + '</span>';
								jQuery(form).find('[name=' + key + ']').closest('.mod-form-group').addClass("has-error").append(errors);
							}*/
						}
					}

				} else if ('success' == data.status) {

					//jQuery(form).children('.error_text').html('');
					//jQuery(form).find('.msg-block').html(data.message);
					jQuery.wsh.resetForm(form);
					//jQuery(form).modal('hide');
					data.message = '<p>' + data.message + '</p>';
					jQuery('#modal-success .modal-header h3').text(data.title);
                    jQuery('#modal-success .modal-body p').remove();
					jQuery('#modal-success .modal-body').html(data.message);

                    $('#modal-success').modal('show');
                    //success();
					/*$.material.init();

					$(window).scroll();
					$(window).resize();
					*/

                    //$('.modal').modal('hide');
					//$('#popap-successfully').modal('show');

					//jQuery(form).find('.rate_box .checked').removeClass('checked');
				}
			},
			complete: function(xhr) {
				//jQuery(form).loadunmask();
				jQuery(form).removeClass('loading');
			}
		});

		return false;
	},

	loadMore: function(btn)
	{
		jQuery.ajax({
			type: 'POST',
			url: jQuery(btn).parent().attr('data-ajaxurl'),
			//data: jQuery(form).serialize(),
			dataType: 'json',
			beforeSend: function(xhr) {
				jQuery(btn).addClass('loading');
			},
			success: function(data) {
				jQuery(btn).parent().remove();
				jQuery(jQuery(btn).parent().attr('data-conteiner')).append(data.html);
			},
			complete: function(xhr) {
				jQuery(btn).removeClass('loading');
			}
		});

		return false;
	},

	resetForm: function(form)
	{
		jQuery(form).find('.rating .rate-star > div.checked').removeClass('checked');
		form.reset();
        $(form).find('.error').removeClass('error');

		$(form).find('.label-floating').each(function(){

			$(this).addClass('is-empty');

		});



	}

};

