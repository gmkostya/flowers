<div class="feedback-form__block">
    <p><?php echo $element['form_text']['title'] ?></p>
    <span><?php echo $element['form_text']['text'] ?></span>
    <form class="form-horizontal oc-custom-forms" id="feedback-form" method="post" action="<?php echo $action; ?>">
        <div style="display:none">
            <input type="text" name="name" value="" />
            <input type="checkbox" name="confirm" value="1" <?php echo ((isset($confirm) && $confirm != '')? 'checked="checked"':'')?> />
        </div>
        <input type="text" name="first_name" id="input-name" placeholder="<?php echo $element['first_name']['placeholder']; ?>" value="<?php echo $element['first_name']['value']; ?>" class="form-control input-form">
        <input type="text" name="phone" id="input-phone" placeholder="<?php echo $element['phone']['placeholder']; ?>" value="<?php echo $element['phone']['value']; ?>" class="form-control input-form">
        <button class="btn-green btn-green--form text-big" type="submit"><span><?php echo $element['button']['label']; ?></span></button>
        <input name="goal" value="callback_request" type="hidden">
    </form>
    <p class="form-text">У нас также есть страница с готовыми ответами</p>
    <a href="#">Часто задаваемые вопросы</a>
</div>
<?php /*
<div class="modal fade" id="oc_make_order" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"><?php echo $element['form_text']['title'] ?></h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal oc-custom-forms" id="request_call-form" method="post" action="<?php echo $action; ?>">
                    <div style="display:none">
                        <input type="text" name="name" value="" />
                        <input type="checkbox" name="confirm" value="1" <?php echo ((isset($confirm) && $confirm != '')? 'checked="checked"':'')?> />
                    </div>
                    <div class="form-group required">
                        <div class="col-xs-12">
                            <input type="text" name="first_name" id="input-first_name" placeholder="<?php echo $element['first_name']['placeholder']; ?>" value="<?php echo $element['first_name']['value']; ?>" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group required">
                        <div class="col-xs-12">
                            <input type="text" name="phone" id="input-phone" placeholder="<?php echo $element['phone']['placeholder']; ?>" value="<?php echo $element['phone']['value']; ?>" class="form-control"/>
                        </div>
                    </div>
                    <input name="goal" value="callback_request" type="hidden">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="request_call-btn" data-loading-text="Обработка" class="btn btn-primary"><?php echo $element['button']['label']; ?></button>
            </div>
        </div>
    </div>
</div>*/?>