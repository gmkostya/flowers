<div class="banner-form">
    <div class="banner-form__block">
        <p><?php echo $element['form_text']['title'] ?></p>
        <span><?php echo $element['form_text']['text'] ?></span>
        <form class="form-horizontal oc-custom-forms" id="banner-form" method="post" action="<?php echo $action; ?>">
            <div style="display:none">
                <input type="text" name="name" value="" />
                <input type="checkbox" name="confirm" value="1" <?php echo ((isset($confirm) && $confirm != '')? 'checked="checked"':'')?> />
            </div>
            <input type="text" name="phone1" id="input-name" placeholder="<?php echo $element['phone1']['placeholder']; ?>" value="<?php echo $element['phone1']['value']; ?>" class="form-control input-form">
            <input type="text" name="phone2" id="input-phone" placeholder="<?php echo $element['phone2']['placeholder']; ?>" value="<?php echo $element['phone2']['value']; ?>" class="form-control input-form">
            <button class="btn-green btn-green--form text-big" type="submit"><span><?php echo $element['button']['label']; ?></span></button>
            <input name="goal" value="callback_request" type="hidden">
        </form>
        <p class="form-text">
            *отправляя форму Вы подтверждаете свое согласие на обработку ваших данных.<br>
            ** Мы гарантируем, что ваши данные не попадут третьим лицам.
        </p>
    </div>
</div>
<?php /*
<section class="green-form">
    <form class="container callback wsh-form" method="post" action="<?php echo $action; ?>">
        <div class="img"><img src="<?php echo $element['image']['thumbnail'] ?>" alt=""/></div>
        <h3 class="green-title"><?php echo $element['form_text']['title'] ?></h3>
        <div style="display:none">
            <input type="text" name="name" value="" />
            <input type="checkbox" name="confirm" value="1" <?php echo ((isset($confirm) && $confirm != '')? 'checked="checked"':'')?> />
        </div>
        <div class="form-group">
            <div class="form-control line-wrap">
                <input type="text" id="first_name" name="first_name" placeholder="<?php echo $element['first_name']['placeholder']; ?>" value="<?php echo $element['first_name']['value']; ?>" />
            </div>
            <div class="form-control line-wrap">
                <input type="tel" id="phone" name="phone" placeholder="<?php echo $element['phone']['placeholder']; ?>" value="<?php echo $element['phone']['value']; ?>" />
            </div>
            <div class="form-control">
                <button type="submit" class="btn" <?php echo $element['button']['onclick'] ? ' onclick="' . $element['button']['onclick'] . '"' : ''; ?>><span class="help"><?php echo $element['button']['label']; ?></span></button>
            </div>
        </div>
    </form>
</section>*/?>