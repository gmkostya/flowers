<div class="modal fade" id="oc_make_order" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title"><?php echo $element['form_text']['title'] ?></h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal oc-custom-forms" id="request_call-form" method="post" action="<?php echo $action; ?>">
                    <div style="display:none">
                        <input type="text" name="name" value="" />
                        <input type="checkbox" name="confirm" value="1" <?php echo ((isset($confirm) && $confirm != '')? 'checked="checked"':'')?> />
                    </div>
                    <div class="form-group required">
                        <div class="col-xs-12">
                            <input type="text" name="first_name" id="input-first_name" placeholder="<?php echo $element['first_name']['placeholder']; ?>" value="<?php echo $element['first_name']['value']; ?>" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group required">
                        <div class="col-xs-12">
                            <input type="text" name="phone" id="input-phone" placeholder="<?php echo $element['phone']['placeholder']; ?>" value="<?php echo $element['phone']['value']; ?>" class="form-control"/>
                        </div>
                    </div>
                    <input name="goal" value="callback_request" type="hidden">
                </form>
            </div>
            <div class="modal-footer">
              <button type="submit" id="request_call-btn" data-loading-text="Обработка" class="btn-green btn-green--form text-big"><span><?php echo $element['button']['label']; ?></span></button>
            </div>
        </div>
    </div>
</div>
<?php /*
<div class="modal fade" id="modalCallback" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <div class="modal-content">
            <div class="modal-header">
                <h3><?php echo $element['form_text']['title'] ?></h3>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo $action; ?>" class="callback wsh-form">
                    <div style="display:none">
                        <input type="text" name="name" value="" />
                        <input type="checkbox" name="confirm" value="1" <?php echo ((isset($confirm) && $confirm != '')? 'checked="checked"':'')?> />
                    </div>
                    <div class="form-group">
                        <div class="form-control line-wrap">
                            <input type="text" id="first_name" name="first_name" placeholder="<?php echo $element['first_name']['placeholder']; ?>" value="<?php echo $element['first_name']['value']; ?>" />
                        </div>
                        <div class="form-control line-wrap">
                            <input type="tel" id="phone" name="phone" placeholder="<?php echo $element['phone']['placeholder']; ?>" value="<?php echo $element['phone']['value']; ?>" />
                        </div>
                        <div class="form-control">
                            <button type="submit" class="btn" <?php echo $element['button']['onclick'] ? ' onclick="' . $element['button']['onclick'] . '"' : ''; ?>><span class="help"><?php echo $element['button']['label']; ?></span></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>*/?>
