<?php echo $header; ?>
<div class="container" xmlns="http://www.w3.org/1999/html">
  <ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
      <?php foreach ($breadcrumbs as $i => $breadcrumb) { ?>
        <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
            <?php if ($i + 1 < count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
            <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
            <?php } ?>
        </li>
      <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
          <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
          <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
          <?php $class = 'col-sm-12'; ?>
      <?php } ?>
    <div id="content" class="contacts-us <?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="title-primary"><?php echo $contact_info['meta_h1']; ?></h1>

      <div class="contacts-us__info">

        <?php echo $contact_info_description; ?>
          <?php if (0) { ?>

            <div class="row">
              <div class="col-sm-3"><h3 class="contacts-us-phone">Телефон</h3><p><a href="tel:88001234567" style="font-size: 26px;">8 800 123 45 67</a></p></div>
              <div class="col-sm-3"><h3 class="contacts-us-address">Адрес</h3><p>г. Краснодар,<br>ул. Ленина, д. 110, оф.1</p></div>
              <div class="col-sm-3"><h3 class="contacts-us-time">Часы работы</h3><p>с 9.00 до 22.00 <br><span style="font-size: 14px;line-height: 24px;">Понедельник - Воскресенье <br>без обеда</span></p></div>
              <div class="col-sm-3"><h3 class="contacts-us-email">E-mail</h3><p><a href="mailto:mil@mail.ru">mil@mail.ru</a></p></div>
            </div>

          <?php } ?>

        <address style="display: none">
          <div itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="name">название сайта</span><br/>
            <span itemprop="postalCode">индекс,</span>
            <span itemprop="addressLocality">страна, область, город</span> <span
              itemprop="streetAddress">название улицы</span><br/>
            Телефон: <span itemprop="telephone">телефон</span><br/>
            скайп: <span itemprop="contactOption">скайп</span><br/>
            E-mail: <span itemprop="email">test@test.com</span>
          </div>
            <?php //echo $address; ?>
        </address>
      </div>

      <div class="map">
        <div id="map"></div>
      </div>
      <div class="contacts-us__feedback">
        <div class="contacts-us__feedback-block">
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <h4><?php echo $text_contact; ?></h4>
            <div class="form-group required row">
              <div class="col-sm-6">
                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name"
                       class="form-control input-form" placeholder="<?php echo $entry_name; ?>"/>
                  <?php if ($error_name) { ?>
                    <div class="text-danger"><?php echo $error_name; ?></div>
                  <?php } ?>
                <input type="text" name="phone" value="<?php echo $telephone; ?>" id="input-phone"
                       class="form-control input-form" placeholder="<?php echo $entry_phone; ?>"/>
<!--                  --><?php //if ($error_telephone) { ?>
<!--                    <div class="text-danger">--><?php //echo $error_telephone; ?><!--</div>-->
<!--                  --><?php //} ?>
                  <?php echo $captcha; ?>
              </div>
              <div class="col-sm-6">
                <textarea name="enquiry" rows="10" id="input-enquiry"
                          class="form-control textarea-form"
                          placeholder="<?php echo $text_comment; ?>"><?php echo $enquiry; ?></textarea>
                  <?php if ($error_enquiry) { ?>
                    <div class="text-danger"><?php echo $error_enquiry; ?></div>
                  <?php } ?>
              </div>
            </div>
            <button class="btn-green btn-green--form text-big" type="submit"/>
            <span><?php echo $button_submit; ?></span></button>
          </form>
        </div>
      </div>

        <?php echo $content_bottom; ?>
        <?php echo $column_right; ?></div>
  </div>
</div>

<?php echo $content_bottom2; ?>
<?php echo $footer; ?>

<script type="text/javascript"><!--

    var map;
    var marker;
    var uluru = {lat: 45.019365, lng: 38.982047};

    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: uluru,
            zoom: 15,
            styles: []
        });
        marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }

    //--></script>
<script async="" defer=""
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA3M1rIN8e8jm8YXg2gdueDYTcckjoiWNQ&callback=initMap"></script>

