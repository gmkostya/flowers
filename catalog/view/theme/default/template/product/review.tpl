<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<table class="table">
  <tr>
    <td>
      <strong><?php echo $review['author']; ?></strong>
      <span class="text-right review__date"><?php echo $review['date_added']; ?></span>
      <p class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($review['rating'] < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x star-orange"></i></span>
              <?php } ?>
          <?php } ?></p>
    </td>
  </tr>
  <tr>
    <td colspan="2"><p class="review__text"><?php echo $review['text']; ?></p></td>
  </tr>
</table>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
