<?php echo $header; ?>
<div class="container">
    <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
  <h1 class="title-primary"><?php echo $heading_title; ?></h1>
  <div class="row" class="">
      <?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
          <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
          <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
          <?php $class = 'col-sm-12'; ?>
      <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="category">
          <?php if ($categories) { ?>
            <div class="category-module">
              <div class="row">
                  <?php foreach ($categories as $category) { ?>
                    <div class="item col-sm-4">
                      <a href="<?php echo $category['href']; ?>" class="category-module__title"><?php echo $category['name']; ?></a>
                      <img src="<?php echo $thumb; ?>" alt="<?php echo $category['name']; ?>" class="category-module__image">
                    </div>
                  <?php } ?>
              </div>
            </div>
          <?php } ?>
          <?php if ($products) { ?>
            <div class="row category__btn">
              <div class="col-sm-6">
                <div class="btn-group hidden-xs">
                  <button type="button" id="large-view" class="btn btn-default btn-sort" data-toggle="tooltip"
                          title="<?php echo $button_large; ?>"><i class="fa fa-th-large"></i></button>
                  <button type="button" id="list-view" class="btn btn-default btn-sort" data-toggle="tooltip"
                          title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
                  <button type="button" id="grid-view" class="btn btn-default btn-sort" data-toggle="tooltip"
                          title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
                </div>
              </div>
              <div class="col-md-6 text-right">
                <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                <select id="input-sort" class="js-select input-sort" onchange="location = this.value;">
                    <?php foreach ($sorts as $sorts) { ?>
                        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                        <option value="<?php echo $sorts['href']; ?>"
                                selected="selected"><?php echo $sorts['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
              </div>
            </div>
            <br/>
            <div class="row">
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item', array('product' => $product, 'button_cart' => $button_cart)); ?>
                <?php } ?>
            </div>
          <?php } ?>
          <?php if (!$categories && !$products) { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons">
              <div class="pull-right"><a href="<?php echo $continue; ?>"
                                         class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
          <?php } ?>
      </div>
        <?php echo $column_right; ?>
    </div>
  </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <hr>
    </div>
  </div>
  <div class="row clearfix navigation-panel">
    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
    <div class="col-sm-6 text-right">
      <select id="input-limit" class="js-select input-limit" onchange="location = this.value;">
          <?php foreach ($limits as $limits) { ?>
              <?php if ($limits['value'] == $limit) { ?>
              <option value="<?php echo $limits['href']; ?>"
                      selected="selected"><?php echo $limits['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
              <?php } ?>
          <?php } ?>
      </select>
    </div>
  </div>
  <div class="row clearfix seo-block">
    <div class="col-sm-8">
      <h4 class="box-heading">Блок сео с фотографией или без</h4>
      <p>Ситуации бывают разные. Возможно, вы просто не успели купить подарок, задержавшись на работе, а может попросту
         не захотели мокнуть под дождём или снегом, чтобы выбрать что-то к празднику. Хорошим решением станет доставка
         букетов онлайн, которая поможет сделать подарок за 5 минут, даже не выходя для этого на улицу.</p>

      <p>Магазин онлайн доставки цветов Flower Studio предлагает вам выбрать из более 250 цветочных композиций, более 10
         видов цветов и ещё большего количества расцветок. У нас вы подберёте букеты на свадьбу, День Рождение,
         корпоратив или юбилей, получив при этом букет уже через два часа после заказа.</p>
    </div>
    <div class="col-sm-4">
      <img src="image/article.jpg" alt="">
    </div>
  </div>
<?php echo $content_bottom; ?>
</div>


<?php echo $content_bottom2; ?>
<?php echo $footer; ?>
