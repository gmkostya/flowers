<footer class="footer">
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-4">
        <ul class="footer__menu footer__menu--hr">
          <li><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
          <li><a href="<?php echo $informations['7']['href']; ?>" title="<?php echo $informations['7']['title']; ?>"><span><?php echo $informations['7']['title']; ?></span></a></li>
          <li><a href="<?php echo $informations['6']['href']; ?>" title="<?php echo $informations['6']['title']; ?>"><span><?php echo $informations['6']['title']; ?></span></a></li>
          <li><a href="<?php echo $informations['4']['href']; ?>" title="<?php echo $informations['4']['title']; ?>"><span><?php echo $informations['4']['title']; ?></span></a></li>
          <li><a href="<?php echo $informations['8']['href']; ?>" title="<?php echo $informations['8']['title']; ?>"><span><?php echo $informations['8']['title']; ?></span></a></li>
        </ul>
        <ul class="footer__menu">
        <?php if ($categories) { ?>
            <?php foreach ($categories as $category) { ?>
                <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
        <?php } ?>
            <?php /*
          <li><a href="#">Цветы</a></li>
          <li><a href="#">Игрушки</a></li>
          <li><a href="#">Сладкие подарки</a></li>
          <li><a href="#">Подарки на все случаи жизни</a></li>
          <li><a href="#">Комнатные растения</a></li>
          <li><a href="#">Дополнения к растениям</a></li>*/?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-4">
        <div class="footer__logo">
          <a href="<?php echo $home; ?>"><img src="/image/catalog/logo-footer.png" title="" alt="" class="img-responsive" /></a>
        </div>
        <ul class="soc-icons">
          <li><a href="<?php echo $url_vk ?>"><span class="icon-soc icon-vk"></span></a></li>
          <li><a href="<?php echo $url_ok ?>"><span class="icon-soc icon-ok"></span></a></li>
          <li><a href="<?php echo $url_in ?>"><span class="icon-soc icon-instagram"></span></a></li>
        </ul>
      </div>
      <div class="col-sm-4">
        <div class="footer__about-us">
          <h5><?php echo $text_we_there; ?></h5>
            <p class="address"><?php echo $address ?></p><br>
          <p class="phone"><?php echo $phone ?></p><br>
          <p class="email"><?php echo $email ?></p>
        </div>
        <div class="footer__we-work">
          <h5><?php echo $text_we_work; ?></h5>
          <p class="address"><?php echo $open ?></p>
        </div>
        <div class="btn-box">
          <button class="btn-green btn-green--single text-big" type="button" data-toggle="modal" data-target="#oc_make_order"><span>Сделать заказ</span></button>
        </div>
      </div>
    </div>
    <hr>
    <div class="row copyright clearfix">
      <div class="col-sm-6"><a class="terms-of-agreement" href="#">Условия соглашения</a></div>
      <div class="col-sm-6"><span class="copyright__info">Все права защищены (с) ДариЦветыДариПодарки, 2018г.</span></div>
    </div>
  </div>
</footer>
<?php echo $oc_cf_make_order ?>
<script src="catalog/view/javascript/bootstrap-select/bootstrap-select.js" type="text/javascript"></script>
<script src="catalog/view/javascript/scrollbar/jquery.mousewheel.js" type="text/javascript"></script>
<script src="catalog/view/javascript/scrollbar/jquery.jscrollpane.js" type="text/javascript"></script>
<script src="catalog/view/javascript/oc/custom_forms.js" type="text/javascript"></script>
</body></html>
