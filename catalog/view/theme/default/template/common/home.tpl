<?php echo $header; ?>
<main class="main">
  <div id="content" class="">
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-lg-8">
          <div class="banner-home">
            <img src="/image/catalog/banners/banner_home_1.jpg" alt="">
            <a href="#" class="btn-green text-normal"><span>Подробнее</span></a>
          </div>
        </div>
        <div class="col-md-5 col-lg-4">
            <?php echo $oc_cf_order_flowers?>
        <?php /*
          <div class="banner-form">
            <div class="banner-form__block">
              <p>Хотите подарить цветы,<br/>
                 но не знаете, как?</p>
              <span>Мы сделаем это за вас!</span>
              <form class="form-horizontal" id="banner-form">
                    <input type="text" name="name" id="input-name" placeholder="Телефон получателя"
                           class="form-control input-form" required>
                    <input type="text" name="phone" id="input-phone" placeholder="Ваш телефон"
                           class="form-control input-form" required>
                <button class="btn-green btn-green--form text-big" type="submit"><span>Заказать цветы, не зная адреса</span></button>
                <input name="goal" value="callback_request" type="hidden">
              </form>
              <p class="form-text">*отправляя форму Вы подтверждаете свое согласие на обработку ваших данных.<br>
                                   ** Мы гарантируем, что ваши данные не попадут третьим лицам.</p>
            </div>
          </div>*/?>
        </div>
      </div>
    </div>
    <?php echo $content_top; ?>
    <?php echo $column_left; ?>
    <?php echo $column_right; ?>
    <?php echo $content_bottom; ?>

    <div class="about">
      <div class="container">
        <h3 class="title-heading"><span><?php echo $seo_title ?></span></h3>
        <div class="row">
          <div class="col-md-12 about__row">
            <div class="about__content">
                <?php echo $seo_text ?>
            <?php /*
              <p>Добро пожаловать в магазин Flower Studio. У нас вы найдёте более 250 цветочных <a href="#" style="color: #5c980e;">композиций из роз</a>, тюльпанов, лилий и других цветов по самым низким ценам в Краснодаре. В нашем магазине только свежие цветы, которые мы доставляем в течение двух часов сразу же после заказа.</p>

              <p>Выбор букетов, предлагаемых на сайте, действительно огромен. В магазине вы найдёте как небольшие композиции из 15 или 21 цветка, так и роскошные букеты из 151 или 201 розы. Также мы оформляем композиции из цветов, фруктов или сладостей в красивых плетеных корзинках и предлагаем приобрести к ним дополнительные товары: милых плюшевых мишек, конфеты или открытки.</p>

              <h5><b>Наши преимущества:</b></h5>
              <ul>
                <li>самые низкие цены в Краснодаре. В магазине Flower Studio вы найдёте букеты и цветочные композиции стоимостью от 650 руб;</li>
                <li>более 250 самых популярных цветочных композиций. Служба доставки цветов предлагает букеты из роз, хризантем, тюльпанов, лилий, экзотических орхидей или эквадорских роз. Также мы делаем различные варианты оформления букетов, начиная от флористической сетки и бумаги и заканчивая изящными подарочными корзинками;</li>
                <li>только свежие цветы, срезанные максимум 2 часа назад. Мы не продаём увядающие букеты, снятые с витрины. Вы можете рассчитывать, что купленные у нас цветы с доставкой простоят действительно долго;</li>
                <li>доставка цветов онлайн в течении двух часов. Мы работаем круглосуточно, доставляя букеты по Краснодару и Краснодарскому краю.</li>
              </ul>*/?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="feedback">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="feedback-form">
                <?php echo $oc_cf_feedback ?>
                <?php /*
              <div class="feedback-form__block">
                <p>У вас остались вопросы?</p>
                <span>Мы готовы на них ответить</span>
                <form class="form-horizontal" id="feedback-form">
                  <input type="text" name="name" id="input-name" placeholder="Ваше имя"
                         class="form-control input-form" required>
                  <input type="text" name="phone" id="input-phone" placeholder="Ваш телефон"
                         class="form-control input-form" required>
                  <button class="btn-green btn-green--form text-big" type="submit"><span>у меня есть вопрос</span></button>
                  <input name="goal" value="callback_request" type="hidden">
                </form>
                <p class="form-text">У нас также есть страница с готовыми ответами</p>
                <a href="#">Часто задаваемые вопросы</a>
              </div>*/?>
            </div>
          </div>
        </div>
      </div>
    </div>

      <?php echo $content_bottom2; ?>
  </div>
</main>
<?php echo $footer; ?>
