<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($meta) { ?>
<meta name="robots" content="<?php echo $meta; ?>"/>
<?php } ?>
<?php if (isset($robots) && $robots) { ?>
<meta name="robots" content="<?php echo $robots; ?>"/>
<?php } ?>
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />

<meta property="url" content="<?php echo $og_url; ?>" />
<meta property="title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php if ($og_image) { ?>
<meta property="image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="image" content="<?php echo $logo; ?>" />
<?php } ?>

<meta itemprop="url" content="<?php echo $og_url; ?>">
<meta itemprop="name" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php if ($og_image) { ?>
<meta itemprop="image" content="<?php echo $og_image; ?>">
<?php } else { ?>
<meta itemprop="image" content="<?php echo $logo; ?>">
<?php } ?>

    <?php if($gtm) { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?
id='+i+dl;f.parentNode.insertBefore(j,f);
 })(window,document,'script','dataLayer','<?php echo $gtm; ?>');</script>
<!-- End Google Tag Manager -->
    <?php } ?>
<?php /* посилань на альтернативні мовні версії сайту в шапці не потрібно, вони повині бути в сайтмапі лише, але покищо залишив*/?>
<?php /*foreach ($alter_lang as $lang=>$href) { ?>
    <link href="<?php echo $href; ?>" hreflang="<?php echo $lang; ?>" rel="alternate" />
<?php }*/ ?>
  <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
  <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
  <link href="catalog/view/javascript/bootstrap-select/bootstrap-select.css" rel="stylesheet" media="screen" />
  <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <link href="catalog/view/javascript/scrollbar/jquery.jscrollpane.css" rel="stylesheet" media="screen" />
  <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
  <link href="catalog/view/javascript/slick/slick.css" rel="stylesheet" media="screen" />
  <script src="catalog/view/javascript/slick/slick.min.js" type="text/javascript"></script>
  <link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
  <link href="catalog/view/theme/default/stylesheet/styles.css" rel="stylesheet">
  <link href="catalog/view/theme/default/stylesheet/wsh.css" rel="stylesheet">
  <link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
  <?php foreach ($styles as $style) { ?>
  <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
  <?php } ?>
  <script src="catalog/view/javascript/common.js" type="text/javascript"></script>

<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Organization",
    "url": "<?php echo $base; ?>",
    "logo": "<?php echo $logo; ?>"
}
</script>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<?php if($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                      height="0" width="0"
                      style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>
<nav id="top" class="top-line">
  <div class="container">
    <div class="row">
      <div id="top-links" class="col-xs-1 col-sm-2 col-md-5 col-lg-6">
        <a href="<?php echo $home; ?>" class="top-links__home"><span class="icon-home"></span></a>
        <button type="button"class="top-links__btn"><i class="fa fa-bars"></i></button>

        <ul class="list-inline top-links__menu">
          <li><a href="<?php echo $informations['7']['href']; ?>" title="<?php echo $informations['7']['title']; ?>"><span><?php echo $informations['7']['title']; ?></span></a></li>
          <li><a href="<?php echo $informations['6']['href']; ?>" title="<?php echo $informations['6']['title']; ?>"><span><?php echo $informations['6']['title']; ?></span></a></li>
          <li><a href="<?php echo $informations['4']['href']; ?>" title="<?php echo $informations['4']['title']; ?>"><span><?php echo $informations['4']['title']; ?></span></a></li>
          <li><a href="<?php echo $informations['8']['href']; ?>" title="<?php echo $informations['8']['title']; ?>"><span><?php echo $informations['8']['title']; ?></span></a></li>
        </ul>
      </div>
      <div class="col-xs-11 col-sm-10 col-md-7 col-lg-6 col-search-cart">
        <div class="row">
          <div class="col-sm-7 col-md-6 top-line__search"><?php echo $search; ?></div>
          <div class="col-sm-5 col-md-6"><?php echo $cart; ?></div>
        </div>
      </div>
    </div>
  </div>`
</nav>

<header class="header">
  <div class="container">
    <div class="row header__wrap">
      <div class="col-sm-6">
        <div id="logo">
          <?php if ($logo) { ?>
          <?php if ($home == $og_url) { ?>
          <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
          <?php } else { ?>
          <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
          <?php } ?>
          <?php } else { ?>
          <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="phone">
          <a href="<?php echo $contact; ?>" id="phone">8 800 234-56-89</a>
          <button class="btn-green btn-green--single text-big" type="button" data-toggle="modal" data-target="#oc_make_order"><span>Сделать заказ</span></button>
        </div>
      </div>
    </div>
  </div>
    <?php echo $slider; ?>
</header>
<?php if ($categories) { ?>
<div class="nav-menu">
  <div class="container">
    <div class="row">
      <nav id="menu" class="navbar">
        <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
          <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav">
              <?php foreach ($categories as $category) { ?>
                  <?php if ($category['children']) { ?>
                  <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                    <div class="dropdown-menu">
                      <div class="dropdown-inner">
                          <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                            <ul class="list-unstyled">
                                <?php foreach ($children as $child) { ?>
                                  <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                          <?php } ?>
                      </div>
                  </li>
                  <?php } else { ?>
                  <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                  <?php } ?>
              <?php } ?>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</div>

<?php } ?>
