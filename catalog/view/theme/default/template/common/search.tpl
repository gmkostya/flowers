<form id="search" class="input-group search">
  <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control" />
  <button type="submit"><span class="icon-search"></span></button>
</form>
