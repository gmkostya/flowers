<div class="products">
  <div class="container">
    <h3 class="title-heading"><span><?php echo $heading_title; ?></span></h3>
    <div class="row">
      <div class="col-sm-12">
        <div class="newsletter--scroll scroll">
            <?php foreach ($products as $product) { ?>
                <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
            <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
