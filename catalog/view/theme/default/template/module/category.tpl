<div class="category-module">
  <div class="container">
    <div class="row">
      <?php foreach ($categories as $category) { ?>
        <div class="item col-sm-6 col-md-4">
          <a href="<?php echo $category['href']; ?>" class="category-module__title"><?php echo $category['name']; ?></a>
          <img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" class="category-module__image">
        </div>
      <?php } ?>
    </div>
  </div>
</div>
