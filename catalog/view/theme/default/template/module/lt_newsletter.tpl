<div class="newsletter">
  <div class="container">
    <div class="row" id="newsletter">
      <div class="col-sm-12">
        <div class="newsletter-form">
          <div class="feedback-form__block">
            <p>Хотите быть в курсе новых акций и предложений?<br>
              Подписывайтесь на нашу рассылку!</p>
            <form class="form-horizontal" id="lt_newsletter_form">
              <input type="text" name="phone" id="input-phone" placeholder="Ваш телефон" class="form-control input-form">
              <button class="btn-green btn-green--form text-big" type="submit"><span>подписаться на sms-новости</span></button>
              <input name="goal" value="callback_request" type="hidden">
            </form>
            <p class="form-text">*отправляя форму Вы подтверждаете свое согласие на обработку ваших данных.<br>
                                 ** Мы гарантируем, что ваши данные не попадут третьим лица</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
		$(document).ready(function($) {
			$('#lt_newsletter_form').submit(function(){
				$.ajax({
					type: 'post',
					url: '<?php echo $action; ?>',
					data:$("#lt_newsletter_form").serialize(),
					dataType: 'json',
					beforeSend: function() {
						$('.btn-newsletter').attr('disabled', true).button('loading');
					},
					complete: function() {
						$('.btn-newsletter').attr('disabled', false).button('reset');
					},
					success: function(json) {
						$('.alert, .text-danger').remove();
						$('.form-group').removeClass('has-error');

						if (json.error) {
							$('#lt_newsletter_form').after('<div class="alert alert-danger newsletter-msg">' + json.error + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
						} else {
							$('#lt_newsletter_form').after('<div class="alert alert-success newsletter-msg">' + json.success + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
							$('#lt_newsletter_email').val('');
						}
					}

				});
				return false;
			});
		});
	//--></script>
