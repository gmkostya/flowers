<?php echo $header;
$s = 1; ?>
<div class="container">
    <?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
  <div class="row"><?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
          <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
          <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
          <?php $class = 'col-sm-12'; ?>
      <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <?php if (!empty($sections)): ?>
          <div class="faq-area">
              <?php $i = 0; ?>
              <?php foreach ($sections as $section): ?>
                  <?php if (!empty($section['items'])): ?>
                  <h2 class="title-primary" style="padding: 0 15px;"><?php echo $section['title']; ?></h2>
                  <div class="faq-section">
                      <?php $s = 1; ?>
                      <?php foreach ($section['items'] as $item): ?>
                          <?php if (trim($item['question']) == '') continue; ?>
                        <div class="faq-item">
                          <div class="faq-question">
                            <h4 class="faq-question__title">

                              <span><?php echo $item['question']; ?></span>
                            </h4>
                          </div>
                          <div class="faq-answer">
                            <div class="faq-answer__text">
                                <?php echo $item['answer']; ?>
                            </div>
                          </div>

                        </div>

                          <?php $i++;
                          $s++; ?>
                      <?php endforeach; ?>
                  </div>
                  <?php endif; ?>
              <?php endforeach; ?>
          </div>
        <?php endif; ?>

        <?php echo $column_right; ?></div>
  </div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
