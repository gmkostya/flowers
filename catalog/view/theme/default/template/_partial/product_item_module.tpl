<div class="products__item">
  <div class="image">
    <a href="<?php echo $product['href']; ?>">
      <img src="<?php echo $product['thumb']; ?>"
           alt="<?php echo $product['name']; ?>"
           title="<?php echo $product['name']; ?>"
           class="img-responsive"/>
    </a>
  </div>
  <span class="label-item label-item--hit">Хит</span>
  <div class="caption">
    <h4>
      <a href="<?php echo $product['href']; ?>">
          <?php echo $product['name']; ?>
      </a>
    </h4>
      <?php if ($product['price']) { ?>
        <p class="products__price">
            <?php if (!$product['special']) { ?>
              <br>
                <?php echo $product['price']; ?>
            <?php } else { ?>
              <span class="products__price-old"><?php echo $product['price']; ?></span>
              <span class="products__price-new"><?php echo $product['special']; ?></span>
            <?php } ?>
        </p>
      <?php } ?>
    <div class="button-group">
      <a href="<?php echo $product['href']; ?>">Подробнее</a>
      <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn-green">
        <span><?php echo $button_cart; ?></span></button>
    </div>
  </div>
</div>
