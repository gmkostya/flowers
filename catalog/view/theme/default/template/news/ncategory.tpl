<?php if ($is_category) { ?>
  <div class="row">
    <div class="col-md-12">
        <?php if ($ncategories) { ?>
          <ul class="bnews__category-list">
              <?php foreach ($ncategories as $ncategory) { ?>
                  <?php if ($ncategory_id == $ncategory['ncategory_id']) { ?>
                  <li class="active"><span><?php echo $ncategory['name']; ?></span></li>
                  <?php } else { ?>
                  <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                  <?php } ?>
              <?php } ?>
          </ul>
        <?php } ?>
    </div>
  </div>
<?php } ?>

<?php if ($article) { ?>
	<div class="bnews-list row<?php if ($display_style) { ?> bnews-list-2<?php } ?>">
		<?php foreach ($article as $articles) { ?>
			<div class="artblock artblock-item col-sm-6 col-md-4<?php if ($display_style) { ?> artblock-2<?php } ?>">
        <div class="artblock__image">
            <?php if ($articles['thumb']) { ?>
              <a href="<?php echo $articles['href']; ?>" class="article-image"><img align="left" src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?>" />
                  <?php if ($articles['date_added']) { ?>
                    <span class="date"><?php echo $articles['date_added']; ?></span>
                  <?php } ?>
              </a>
            <?php } ?>
        </div>
          <?php if ($articles['name']) { ?>
            <div class="name artblock__title"><a href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a></div>
          <?php } ?>
				<?php if ($articles['description']) { ?>
					<div class="description artblock__description"><?php echo $articles['description']; ?></div>
				<?php } ?>

				<?php if ($articles['button']) { ?>
					<div class="blog-button">
            <a class="button artblock__read-more" href="<?php echo $articles['href']; ?>"><?php echo $button_more; ?></a>
              <?php if ($articles['category']) { ?>
                <?php echo $articles['category']; ?>
              <?php } ?>
          </div>
				<?php } ?>
			</div>
		<?php } ?>
  </div>
  <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  </div>
	<script type="text/javascript"><!--
	$(document).ready(function() {
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
		$(window).resize(function() {
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
		});
	});
	//--></script>
<?php } ?>
<?php if ($is_category) { ?>
  <?php if (!$ncategories && !$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } else { ?>
  <?php if (!$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } ?>
<?php if ($disqus_status) { ?>
<script type="text/javascript">
var disqus_shortname = '<?php echo $disqus_sname; ?>';
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
<?php } ?>
<?php if ($fbcom_status) { ?>
<script type="text/javascript">
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $fbcom_appid; ?>',
		  status     : true,
          xfbml      : true,
		  version    : 'v2.0'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>
