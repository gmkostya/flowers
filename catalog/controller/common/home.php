<?php
class ControllerCommonHome extends Controller {
	public function index() {

		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

         $this->config->get('config_meta_title');


		if (isset($this->request->get['route'])) {
			$this->document->addLink(((isset($this->request->server['HTTPS']) && !empty($this->request->server['HTTPS'])) ? HTTPS_SERVER : HTTP_SERVER), 'canonical');
		}

        $this->load->model('catalog/information');
        $information_info = $this->model_catalog_information->getInformationSeo(10);
        $data['seo_title'] = $information_info['title'];
        $data['seo_text'] = html_entity_decode($information_info['description']);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['content_bottom2'] = $this->load->controller('common/content_bottom2');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

        $data['oc_cf_order_flowers'] = $this->load->controller('oc/custom_forms',array('form' => 'order_flowers'));
        $data['oc_cf_feedback'] = $this->load->controller('oc/custom_forms',array('form' => 'feedback'));

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/home.tpl', $data));
		}
	}
}
