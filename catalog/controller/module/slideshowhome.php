<?php
class ControllerModuleSlideshowhome extends Controller {
	public function index() {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner('7');

        foreach ($results as $result) {
            if (is_file(DIR_IMAGE . $result['image'])) {
                $data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], '1920', '525'),
					'origin_image' => 'image/'.$result['image']
				);
            }
        }

        $data['module'] = $module++;

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/slideshowhome.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/slideshowhome.tpl', $data);
		} else {
			return $this->load->view('default/template/module/slideshowhome.tpl', $data);
		}
	}
}
