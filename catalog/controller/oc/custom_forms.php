<?php

class ControllerOcCustomForms extends Controller {

    private $_helper  = 'oc';
    private $_type	  = 'custom_forms';
    private $_module  = 'oc_custom_forms';
    private $error = array();

    public function index($setting) {

        $wsh = new WSH($this->_helper,$this->_type,$this->_helper);

//        $data['element'] = wsh_catalog('forms',$setting['form'],$this->config->get('config_language_id'));
        $data['element'] = $wsh->wsh_get_group_language($setting['form'],$this->config->get('config_language_id'));
        /*echo '<pre>';
        print_r($data['element']);
        echo '</pre>';*/
        /*if($setting['form'] == 'callback_page'){
            echo '<pre>';
            print_r($data['element']);
            die();
        }*/
        $data['action'] = $this->url->link($this->_helper.'/'.$this->_type.'/action_' . $setting['form'], '', 'SSL');

        if (method_exists($this, 'a'.$setting['form'])) {
            $data['values'] = $this->{'a'.$setting['form']}($data);
        }
        /*$this->load->model('tool/image');
        $data['element']['image']['thumbnail'] = $this->model_tool_image->resize($data['element']['image']['value'], 152, 152, 'onesize');*/

        $view = '';
        if(isset($setting['view'])){
            $view = '_' . $setting['view'];
        }
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/'.$this->_helper.'/'.$this->_type.'/'.$setting['form'].$view.'.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/'.$this->_helper.'/'.$this->_type.'/'.$setting['form'].$view.'.tpl', $data);
        } else {
            return $this->load->view('default/template/wsh/forms/base.tpl', $data);
        }
    }
    
    public function sendData($data, $from, $sender, $fields, $form_name = '') {
        /*begin templates email*/
        $vars = array();

        $text = '';
        /*echo '<pre>';
        var_dump($fields);
        exit();*/
        /*if(isset($data['values'])){
            $labels = wsh_catalog('forms','calculate',$this->config->get('config_language_id'));
            $values = json_decode($data['values'],true);
            foreach ($labels as $key => $item) {
                if ((isset($item['mail_send']) && $item['mail_send']) && isset($values[$key])) {
                    $text .= $item['label'] . ": " . $values[$key] . "\n\n";
                    $vars['fields'][$key] = array(
                        'label' => $item['label'],
                        'value' => $data[$key]
                    );
                }
            }
        }*/
        foreach ($fields as $key => $item) {
            if ((isset($item['mail_send']) && $item['mail_send']) && isset($data[$key])) {
                if(!isset($data['values'])){
                    $text .= $item['label'] . ": " . $data[$key] . "\n\n";
                    $vars['fields'][$key] = array(
                        'label' => $item['label'],
                        'value' => $data[$key]
                    );
                }
            }
        }
        $subject = 'Сообщения с формы -' .$form_name;

        $vars['form_name'] = $form_name;

        //$template = $this->ocstore->getTemplateEmail('forms', $vars);
        //$text = $template['text'];
        //$html = $template['html'];
        //$subject = $template['subject'];
        /*end templates email*/
        
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender($sender);
        $mail->setSubject($subject);
        $mail->setText(strip_tags(html_entity_decode($text, ENT_QUOTES, 'UTF-8')));
        $mail->send();

        //$mail->setFrom($from);
        //$mail->setSender($sender);
        //$mail->setSubject($subject);
        //$mail->setText(strip_tags(html_entity_decode($this->request->post['enquiry'], ENT_QUOTES, 'UTF-8')));

        //$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
        //$mail->setText(strip_tags(html_entity_decode($text, ENT_QUOTES, 'UTF-8')));

        //$mail->send();
        
        // Send to additional alert emails
        $emails = explode(',', $this->config->get('config_alert_emails'));

        foreach ($emails as $email) {
            if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
                $mail->setTo($email);
                $mail->send();
            }
        }
        
    }

    public function action_make_order(){
        $json = array('status' => 'error');
        $wsh = new WSH($this->_helper,$this->_type,$this->_helper);
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $wsh->wsh_get_group_language('make_order',$this->config->get('config_language_id'));

            $errors = $this->validate_make_order($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, $field['form_text']['title']);
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function amake_order($data){

        return $data;
    }

    protected function validate_make_order($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 16) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function action_order_flowers(){
        $json = array('status' => 'error');
        $wsh = new WSH($this->_helper,$this->_type,$this->_helper);
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $wsh->wsh_get_group_language('order_flowers',$this->config->get('config_language_id'));

            $errors = $this->validate_order_flowers($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, $field['form_text']['title']);
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function aorder_flowers($data){

        return $data;
    }

    protected function validate_order_flowers($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['phone1']['required']){
            if (utf8_strlen($this->request->post['phone1']) != 16) {
                $this->error['phone1'] = $data['phone1']['error'];
            }
        }

        if($data['phone2']['required']){
            if (utf8_strlen($this->request->post['phone2']) != 16) {
                $this->error['phone2'] = $data['phone2']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function action_feedback(){
        $json = array('status' => 'error');
        $wsh = new WSH($this->_helper,$this->_type,$this->_helper);
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $wsh->wsh_get_group_language('make_order',$this->config->get('config_language_id'));

            $errors = $this->validate_make_order($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, $field['form_text']['title']);
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function afeedback($data){

        return $data;
    }

    protected function validate_feedback($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 16) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

}

?>
