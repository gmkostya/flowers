<?php

class ControllerWshForms extends Controller {

    private $error = array();
    
    public function __construct($registry) {
        parent::__construct($registry);
    }
    
    public function index($setting) {

        $this->load->helper('wsh');
        $data['element'] = wsh_catalog('forms',$setting['form'],$this->config->get('config_language_id'));

        /*if($setting['form'] == 'callback_page'){
            echo '<pre>';
            print_r($data['element']);
            die();
        }*/
        $data['action'] = $this->url->link('wsh/forms/action_' . $setting['form'], '', 'SSL');

        if (method_exists($this, 'a'.$setting['form'])) {
            $data['values'] = $this->{'a'.$setting['form']}($data);
        }
        $this->load->model('tool/image');
        $data['element']['image']['thumbnail'] = $this->model_tool_image->resize($data['element']['image']['value'], 152, 152, 'onesize');

        $view = '';
        if(isset($setting['view'])){
            $view = '_' . $setting['view'];
        }
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/wsh/forms/'.$setting['form'].$view.'.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/wsh/forms/'.$setting['form'].$view.'.tpl', $data);
        } else {
            return $this->load->view('default/template/wsh/forms/base.tpl', $data);
        }
    }
    
    public function sendData($data, $from, $sender, $fields, $form_name = '') {
        /*begin templates email*/
        $vars = array();

        $text = '';
        /*echo '<pre>';
        var_dump($fields);
        exit();*/
        if(isset($data['values'])){
            $labels = wsh_catalog('forms','calculate',$this->config->get('config_language_id'));
            $values = json_decode($data['values'],true);
            foreach ($labels as $key => $item) {
                if ((isset($item['mail_send']) && $item['mail_send']) && isset($values[$key])) {
                    $text .= $item['label'] . ": " . $values[$key] . "\n\n";
                    $vars['fields'][$key] = array(
                        'label' => $item['label'],
                        'value' => $data[$key]
                    );
                }
            }
        }
        foreach ($fields as $key => $item) {
            if ((isset($item['mail_send']) && $item['mail_send']) && isset($data[$key])) {
                if(!isset($data['values'])){
                    $text .= $item['label'] . ": " . $data[$key] . "\n\n";
                    $vars['fields'][$key] = array(
                        'label' => $item['label'],
                        'value' => $data[$key]
                    );
                }
            }
        }
        $subject = 'Сообщения с формы -' .$form_name;

        $vars['form_name'] = $form_name;

        //$template = $this->ocstore->getTemplateEmail('forms', $vars);
        //$text = $template['text'];
        //$html = $template['html'];
        //$subject = $template['subject'];
        /*end templates email*/
        
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

        $mail->setTo($this->config->get('config_email'));
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender($sender);
        $mail->setSubject($subject);
        $mail->setText(strip_tags(html_entity_decode($text, ENT_QUOTES, 'UTF-8')));
        $mail->send();

        //$mail->setFrom($from);
        //$mail->setSender($sender);
        //$mail->setSubject($subject);
        //$mail->setText(strip_tags(html_entity_decode($this->request->post['enquiry'], ENT_QUOTES, 'UTF-8')));

        //$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
        //$mail->setText(strip_tags(html_entity_decode($text, ENT_QUOTES, 'UTF-8')));

        //$mail->send();
        
        // Send to additional alert emails
        $emails = explode(',', $this->config->get('config_alert_emails'));

        foreach ($emails as $email) {
            if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
                $mail->setTo($email);
                $mail->send();
            }
        }
        
    }

    public function action_callback_modal(){
        $json = array('status' => 'error');

        $this->load->helper('wsh');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = wsh_catalog('forms','callback_modal',$this->config->get('config_language_id'));

            $errors = $this->validate_callback_modal($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, $field['form_text']['title']);
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function acallback_modal($data){

        return $data;
    }

    protected function validate_callback_modal($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 16) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function action_callback_page(){
        $json = array('status' => 'error');

        $this->load->helper('wsh');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = wsh_catalog('forms','callback_page',$this->config->get('config_language_id'));

            $errors = $this->validate_callback_page($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, $field['form_text']['title']);
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function acallback_page($data){

        return $data;
    }

    protected function validate_callback_page($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 16) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function action_calculate(){
        $json = array('status' => 'error');

        $this->load->helper('wsh');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = wsh_catalog('forms','calculate',$this->config->get('config_language_id'));

            $errors = $this->validate_calculate($field);
            if ($errors === true) {

                $json['status'] = 'success';
                //$json['title'] = $field['form_text']['title_successful'];
                //$json['message'] = $field['form_text']['text_successful'];

                $json['values'] = $this->request->post;
                //
                //$from = $this->config->get('config_email');
                //$sender = $this->config->get('config_name');
                //$this->sendData($data, $from, $sender, $field, $field['form_text']['title']);
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function acalculate($data){

        return $data;
    }

    protected function validate_calculate($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        //$select_array = array(1,2,3,4,5,6,7,8,9,);

        for($i=1;$i<=15;$i++){
            if($data['field'.$i]['required']){
                if (empty($this->request->post['field'.$i])) {
                    $this->error['field'.$i] = $data['field'.$i]['error'];
                }
            }
        }


        /*if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 16) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }*/

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function action_calculate_send(){
        $json = array('status' => 'error');

        $this->load->helper('wsh');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = wsh_catalog('forms','calculate_send',$this->config->get('config_language_id'));

            $errors = $this->validate_calculate_send($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, $field['form_text']['title']);
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function acalculate_send($data){

        return $data;
    }

    protected function validate_calculate_send($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 16) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    /*public function action_newsletter(){
        $json = array('status' => 'error');
        $this->load->helper('wsh');
        $this->load->model('module/lt_newsletter');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $this->load->wsh_forms('newsletter');
            $errors = $this->validate_newsletter($field);
            if ($errors === true) {
                $this->model_module_lt_newsletter->subscribe($this->request->post['email']);
                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_subscribed'];
                $json['message'] = $field['form_text']['text_subscribed'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, 'Подписка на новости');
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    protected function validate_newsletter($data){

        if (!$this->request->post['email']) {
            $this->error['email'] = $data['email']['email_incorrect'];
        }

        if($data['email']['required']){
            if (!wsh_validate($this->request->post['email'])) {
                $this->error['email'] = $data['email']['email_incorrect'];
            }
        }

        $this->load->model('module/lt_newsletter');
        $this->load->language('module/lt_newsletter');

        $row = $this->model_module_lt_newsletter->row($this->request->post['email']);
        if($row)
        {
            $this->error['email'] = $data['email']['email_exists'];
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function action_contact(){
        $json = array('status' => 'error');
        $this->load->helper('wsh');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $this->load->wsh_forms('contact');
            $errors = $this->validate_contact($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, 'Обратная связь');

            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    protected function validate_contact($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 16) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if($data['email']['required']){
            if (!wsh_validate($this->request->post['email'])) {
                $this->error['email'] = $data['email']['error'];
            }
        }

        if($data['massage']['required']){
            if (utf8_strlen($this->request->post['massage']) < 3) {
                $this->error['massage'] = $data['massage']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }*/

    public function abuyoneclick($data){

        return $data;
    }

    public function action_buyoneclick(){
        $json = array('status' => 'error');
        $this->load->helper('wsh');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $this->load->wsh_forms('buyoneclick');
            $errors = $this->validate_buyoneclick($field);
            if ($errors === true) {

                // добавить новый заказ
                $total_data = array();
                //$total = 0;
                $taxes = '';//$this->cart->getTaxes();

                $data_order = array();
                $data_order['invoice_prefix'] = $this->config->get('config_invoice_prefix');
                $data_order['store_id'] = $this->config->get('config_store_id');
                $data_order['store_name'] = $this->config->get('config_name');

                $data_order['language_id'] = $this->config->get('config_language_id');
                $data_order['currency_id'] = $this->currency->getId();
                $data_order['currency_code'] = $this->currency->getCode();
                $data_order['currency_value'] = $this->currency->getValue($this->currency->getCode());
                $data_order['ip'] = $this->request->server['REMOTE_ADDR'];

                if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                    $data_order['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
                } elseif(!empty($this->request->server['HTTP_CLIENT_IP'])) {
                    $data_order['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
                } else {
                    $data_order['forwarded_ip'] = '';
                }

                if (isset($this->request->server['HTTP_USER_AGENT'])) {
                    $data_order['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
                } else {
                    $data_order['user_agent'] = '';
                }

                if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                    $data_order['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
                } else {
                    $data_order['accept_language'] = '';
                }

                if ($data_order['store_id']) {
                    $data_order['store_url'] = $this->config->get('config_url');
                } else {
                    $data_order['store_url'] = HTTP_SERVER;
                }
                if ($this->customer->isLogged()) {
                    $data_order['customer_id'] = $this->customer->getId();
                    $data_order['customer_group_id'] = $this->customer->getCustomerGroupId();
                } else {
                    $data_order['customer_id'] = 0;
                    $data_order['customer_group_id'] = 0;
                }

                $data_order['firstname'] = $this->request->post['first_name'];
                $data_order['lastname'] = $this->request->post['last_name'];
                $data_order['email'] = $this->request->post['email'];
                $data_order['telephone'] = $this->request->post['phone'];
                $data_order['fax'] = '';
                $data_order['comment'] = $this->request->post['massage'];

                $data_order['affiliate_id'] = 0;
                $data_order['commission'] = 0;

                $data_order['shipping_firstname'] = '';
                $data_order['shipping_lastname'] = '';
                $data_order['shipping_company'] = '';
                $data_order['shipping_address_1'] = '';
                $data_order['shipping_address_2'] = '';
                $data_order['shipping_city'] = '';//$this->request->post['city'];
                $data_order['shipping_postcode'] = '';
                $data_order['shipping_zone'] = '';
                $data_order['shipping_zone_id'] = '';
                $data_order['shipping_country'] = '';
                $data_order['shipping_country_id'] = '';
                $data_order['shipping_address_format'] = '';
                $data_order['shipping_method'] = '';
                $data_order['shipping_code'] = '';

                $data_order['payment_firstname'] = '';
                $data_order['payment_lastname'] = '';
                $data_order['payment_company'] = '';
                $data_order['payment_company_id'] = '';
                $data_order['payment_tax_id'] = '';
                $data_order['payment_address_1'] = '';
                $data_order['payment_address_2'] = '';
                $data_order['payment_city'] = '';//$this->request->post['city'];
                $data_order['payment_postcode'] = '';
                $data_order['payment_zone'] = '';
                $data_order['payment_zone_id'] = '';
                $data_order['payment_country'] = '';
                $data_order['payment_country_id'] = '';
                $data_order['payment_address_format'] = '';

                $data_order['payment_method'] = '';
                $data_order['payment_code'] = '';

                // ----- product
                $this->load->model('catalog/product');
                $product_info = $this->model_catalog_product->getProduct((int)$this->request->post['oneclick_product_id']);

                if($product_info['special']){
                    $product_price = $product_info['special'];
                }else{
                    $product_price = $product_info['price'];
                }

                $product_data[] = array(
                    'product_id' => $product_info['product_id'],
                    'name'       => $product_info['name'],
                    'model'      => $product_info['model'],
                    'option'     => array(),
                    'download'   => array(),
                    'quantity'   => 1,
                    'subtract'   => '',//$product['subtract'],
                    'price'      => $product_price,
                    'total'      => $product_price,
                    'tax'        => '',//$this->tax->getTax($product['price'], $product['tax_class_id']),
                    'reward'     => ''//$product['reward']
                );

                $total = $product_price;
                $this->load->model('total/total');
                $this->{'model_total_total'}->getTotal($total_data, $total, $taxes);

                $data_order['products'] = $product_data;
                $data_order['totals'] = $total_data;
                $data_order['total'] = $total;
                $data_order['vouchers'] = array();

                if (isset($this->request->cookie['tracking'])) {
                    $data_order['tracking'] = $this->request->cookie['tracking'];

                    $subtotal = $this->cart->getSubTotal();

                    // Affiliate
                    $this->load->model('affiliate/affiliate');

                    $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByCode($this->request->cookie['tracking']);

                    if ($affiliate_info) {
                        $data_order['affiliate_id'] = $affiliate_info['affiliate_id'];
                        $data_order['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
                    } else {
                        $data_order['affiliate_id'] = 0;
                        $data_order['commission'] = 0;
                    }

                    // Marketing
                    $this->load->model('checkout/marketing');

                    $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

                    if ($marketing_info) {
                        $data_order['marketing_id'] = $marketing_info['marketing_id'];
                    } else {
                        $data_order['marketing_id'] = 0;
                    }
                } else {
                    $data_order['affiliate_id'] = 0;
                    $data_order['commission'] = 0;
                    $data_order['marketing_id'] = 0;
                    $data_order['tracking'] = '';
                }

                $data_order['language_id'] = $this->config->get('config_language_id');
                $data_order['currency_id'] = $this->currency->getId();
                $data_order['currency_code'] = $this->currency->getCode();
                $data_order['currency_value'] = $this->currency->getValue($this->currency->getCode());
                $data_order['ip'] = $this->request->server['REMOTE_ADDR'];

                if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
                    $data_order['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
                } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
                    $data_order['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
                } else {
                    $data_order['forwarded_ip'] = '';
                }

                if (isset($this->request->server['HTTP_USER_AGENT'])) {
                    $data_order['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
                } else {
                    $data_order['user_agent'] = '';
                }

                if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
                    $data_order['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
                } else {
                    $data_order['accept_language'] = '';
                }

                //$data['kotik_settings'] = $this->ocstore->get_kotik_settings($this->config, 'shop');

                $this->load->model('checkout/order');

                //$data_order['order_uniqid'] = date('dmy') . '-N' . $this->ocstore->rand_str();

                $order_id = $this->model_checkout_order->addOrder($data_order);
                //$this->model_checkout_order->confirm($order_id, 0);

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];
            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    protected function validate_buyoneclick($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['last_name']['required']){
            if ((utf8_strlen($this->request->post['last_name']) < 3) || (utf8_strlen($this->request->post['last_name']) > 32)) {
                $this->error['last_name'] = $data['last_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 16) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if($data['email']['required']){
            if (!wsh_validate($this->request->post['email'])) {
                $this->error['email'] = $data['email']['error'];
            }
        }

        if($data['massage']['required']){
            if (utf8_strlen($this->request->post['massage']) < 3) {
                $this->error['massage'] = $data['massage']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    /*
    public function action_discount_form(){
        $json = array('status' => 'error');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $this->load->wsh_forms('discount_form');

            //print_r($field);
            //print_r($this->request->post);

            $errors = $this->validate_discount_form($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['title_successful'];
                $json['message'] = $field['text_successful'];

                $data = $this->request->post;

                $callback_data = array(
                    'phone' => $data['phone'],
                    'name' => '',
                    'page_link' => $data['page_url'],
                    'page_name' => $data['page_name']
                );

                $this->load->model('module/kotik_callback_customer');
                $this->model_module_kotik_callback_customer->addCallbackCustomer($callback_data);

                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, 'Не спешите уходить!');

            } else {
                $json['errors'] = $errors;
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function adiscount_form($data){

        return $data;
    }

    protected function validate_discount_form($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 18) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }*/

    public function action_reviews_shop(){
        $json = array('status' => 'error');
        $this->load->helper('wsh');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $this->load->wsh_forms('reviews_shop');
            $errors = $this->validate_reviews($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, 'Оставить отзыв');

                //save review
                $this->load->model('catalog/review');
                $data['name'] = $this->request->post['first_name'];
                $data['text'] = $this->request->post['massage'];
                $data['rating'] = $this->request->post['rating'];
                $data['is_shop_review'] = 1; // отзыв магазина
                $this->model_catalog_review->addReview(0, $data);

            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    public function action_reviews_product(){
        $json = array('status' => 'error');
        $this->load->helper('wsh');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $this->load->wsh_forms('reviews_product');
            $errors = $this->validate_reviews($field);
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, 'Оставить отзыв');

                //save review
                $this->load->model('catalog/review');
                $data['name'] = $this->request->post['first_name'];
                $data['text'] = $this->request->post['massage'];
                $data['rating'] = $this->request->post['rating'];
                $product_id = $this->request->post['product_id'];
                $this->model_catalog_review->addReview($product_id, $data);

            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    protected function validate_reviews($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['email']['required']){
            if (!wsh_validate($this->request->post['email'])) {
                $this->error['email'] = $data['email']['error'];
            }
        }

        if($data['massage']['required']){
            if ((utf8_strlen($this->request->post['massage']) < 3)) {
                $this->error['massage'] = $data['massage']['error'];
            }
        }

        if($data['rating']['required']){
            if (!isset($this->request->post['rating']) || (isset($this->request->post['rating']) && ($this->request->post['rating'] < 1 && $this->request->post['rating'] > 5))) {
                $this->error['rating'] = $data['rating']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function action_vacancies(){
        $json = array('status' => 'error');

        $this->load->helper('wsh');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $field = $this->load->wsh_forms('vacancies');
            $errors = $this->validate_vacancies($field);
            //$directory = realpath(dirname(__FILE__) . '/../../../vacancies');
            $directory = realpath(dirname(DIR_DOWNLOAD . '/vacancies/'));
            $file = $this->uploadFile('filename', $directory,$field);
            if(isset($file['errors']) && !empty($file['errors'])){
                if($errors === true){
                    $errors = array();
                }
                $errors['file'] = $file['errors'];
            }
            if ($errors === true) {

                $json['status'] = 'success';
                $json['title'] = $field['form_text']['title_successful'];
                $json['message'] = $field['form_text']['text_successful'];

                $data = $this->request->post;
                //$data['file'] = (isset($file['name']))? HTTP_SERVER.$file['name']:'';
//                echo '<pre>';
//                print_r($this->request->post);
//                //print_r($this->request->files);
//                print_r($file);
//                print_r($data);
//                die();
                $from = $this->config->get('config_email');
                $sender = $this->config->get('config_name');
                $this->sendData($data, $from, $sender, $field, 'Подать резюме');

            } else {
                $json['errors'] = $errors;
                $json['title'] = $field['form_text']['title_error'];
            }
        }
        $this->response->setOutput(json_encode($json));
    }

    protected function validate_vacancies($data){

        if ((utf8_strlen($this->request->post['name']) > 0)) {
            $this->error['name'] = 'error';
        }

        if (isset($this->request->post['confirm']) && (utf8_strlen($this->request->post['confirm']) == '1')) {
            $this->error['confirm'] = 'error';
        }

        if($data['vacancies']['required']){
            if ($this->request->post['vacancies']=='-') {
                $this->error['vacancies'] = $data['vacancies']['error'];
            }
        }

        if($data['first_name']['required']){
            if ((utf8_strlen($this->request->post['first_name']) < 3) || (utf8_strlen($this->request->post['first_name']) > 32)) {
                $this->error['first_name'] = $data['first_name']['error'];
            }
        }

        if($data['phone']['required']){
            if (utf8_strlen($this->request->post['phone']) != 16) {
                $this->error['phone'] = $data['phone']['error'];
            }
        }

        if($data['email']['required']){
            if (!wsh_validate($this->request->post['email'])) {
                $this->error['email'] = $data['email']['error'];
            }
        }

        if($data['massage']['required']){
            if (utf8_strlen($this->request->post['massage']) < 3) {
                $this->error['massage'] = $data['massage']['error'];
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return $this->error;
        }
    }

    public function uploadFile($key_name, $directory,$data){

        $errors = array();
        $err = 0;

        //if (isset($this->request->files[$key_name]) && $this->request->files[$key_name]['tmp_name']) {

            //$filename = basename(html_entity_decode($this->request->files[$key_name]['name'], ENT_QUOTES, 'UTF-8'));
            $filename = $this->request->post[$key_name];
            //$_path_info = pathinfo($filename);
            //$new_filename = 'x-' . time() . '.' . $_path_info['extension'];

            /*if ((strlen($filename) < 3) || (strlen($filename) > 255)) {
                //$errors[1] = $this->language->get('error_filename');
                $err = 1;
            }

            if (!is_dir($directory)) {
                //$errors[2] = $this->language->get('error_directory');
                $err = 1;
            }

            $file_max_size = 10240000; // 10mb

            if ($this->request->files[$key_name]['size'] > (int)$file_max_size) {
                //$errors[3] = $this->language->get('error_file_size');
                $err = 1;
            }

            $allowed = array(
                'image/jpeg',
                'image/pjpeg',
                'image/png',
                'image/x-png',
                'image/gif',
                'multipart/x-zip',
                'application/zip',
                'application/x-compressed',
                'application/x-zip-compressed',
                'application/rar',
                'application/x-rar-compressed',
                'application/pdf',
                'application/x-pdf',
                'application/x-shockwave-flash'
            );*/

//            if (!in_array($this->request->files[$key_name]['type'], $allowed)) {
//                //$errors[4] = $this->language->get('error_file_type');
//                $err = 1;
//            }

            $allowed = array(
                '.doc',
                '.docx',
                '.rtf',
                '.txt',
                '.odt',
                '.pdf'
            );

            if (!in_array(strtolower(strrchr($filename, '.')), $allowed)) {
                //$errors[4] = $this->language->get('error_file_type');
                $err = 1;
            }

            if($err){
                $errors[1] = $data['file']['error'];
            }

            if ($errors) {
                return array('errors' => implode('<br/>', $errors));
            }

            if (@move_uploaded_file($this->request->files[$key_name]['tmp_name'], $directory . '/' . $new_filename)) {
                return array('name' => $new_filename);
            } else {
                return array('errors' => implode('<br/>', $errors));
            }

        /*} else {
            //$errors[] = $this->language->get('error_file');
            //return $errors;
        }*/

    }
}

?>
